#
# Cool installation.
#


# Installation directories :
# /users/blissadm/python/bliss_modules/

.PHONY: install test


install:
	python setup.py install


test:
	python -m unittest test.full


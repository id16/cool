
**Cool** library contains a set of devices to create an abstraction layer over
the sub control system (Spec, Tango, Taco...)

# Directories

* `cool` contains source code of the library
* `examples` contains small examples. There is no autonomous examples. It
expects to use a Tango device or a Spec session
* `cool.test` contains unit tests

# Unittests

The library use the [default unittest system](https://docs.python.org/2/library/unittest.html)
from Python. To launch the unit tests you can use this kind of commands:

```
python -m unittest cool.test.full                                  # all the tests
python -m unittest cool.test.TestRenew                             # all tests from a module
python -m unittest cool.test.TestRenew.TestRenew                   # all tests from a test case
python -m unittest cool.test.TestRenew.TestRenew.test_same_device  # one specific test
```

# Install

The library contains only source code. There is no need to build anything.

    python setup.py install
    
To install it to the local user space you can use this command.

    python setup.py install --user

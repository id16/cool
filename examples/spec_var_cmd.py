"""Demonstrates how to connect to a spec variable in a remote spec session
"""
import os
import cool
from cool import config
from PyQt5 import Qt
import logging

logging.basicConfig()


class MyMainWindow(Qt.QMainWindow):
    def __init__(self, *args, **kwargs):
        Qt.QMainWindow.__init__(self, *args, **kwargs)

        self.variable = None
        self.cmd = None

        self.central_widget = Qt.QWidget(self)
        self.label = Qt.QLabel("Variable: ", self.central_widget)
        self.txt_newvalue = Qt.QLineEdit(self.central_widget)
        self.cmd_start_cmd = Qt.QPushButton("Execute Command", self.central_widget)

        layout = Qt.QHBoxLayout(self.central_widget)
        layout.addWidget(self.label)
        layout.addWidget(self.txt_newvalue)
        layout.addWidget(self.cmd_start_cmd)
        self.setCentralWidget(self.central_widget)
        self.label.setSizePolicy(Qt.QSizePolicy.MinimumExpanding, Qt.QSizePolicy.Fixed)

        self.txt_newvalue.returnPressed.connect(self.change_variable)
        self.cmd_start_cmd.clicked.connect(self.execute_cmd)

    def variable_changed(self, value):
        self.label.setText("Variable: %s" % value)

    def change_variable(self):
        self.variable.set_value(self.txt_newvalue.text())

    def execute_cmd(self):
        self._cmd_finished = lambda msg: Qt.QMessageBox.information(
            self, "Execute cmd - finished", str(msg)
        )
        self._cmd_failed = lambda msg: Qt.QMessageBox.information(
            self, "Execute cmd - failed", str(msg)
        )
        self._cmd_started = lambda: Qt.QMessageBox.information(
            self, "Execute cmd", "Command started"
        )
        self.cmd.connect("command_finished", self._cmd_finished)
        self.cmd.connect("command_failed", self._cmd_failed)
        self.cmd.connect("command_started", self._cmd_started)
        self.cmd(wait=False, timeout=1)


if __name__ == "__main__":
    app = Qt.QApplication([])

    # make gevent loop working with Qt
    cool.init_qt()

    mainwin = MyMainWindow()
    mainwin.show()

    # set xml files path
    config.set_xml_files_path(os.path.abspath(os.path.dirname(__file__)))

    # create the spec variable object from xml file
    variable = cool.get("spec_var_cmd")

    # get the channel object
    variable_channel = variable.channels["TOTO"]
    cmd = variable.commands["sleep_5_seconds"]
    mainwin.variable = variable_channel
    mainwin.cmd = cmd

    # connect variable object with Qt label
    mainwin.variable_changed(variable_channel.value())
    variable_channel.connect("update", mainwin.variable_changed)

    app.exec_()

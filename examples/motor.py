"""Demonstrates how to connect to a spec motor in a remote spec session
"""
import sys
import cool
from cool import config
from PyQt5 import Qt
import logging

logging.basicConfig()

TANGO_TEMPLATE = """
<object class = "TangoMotor">
    <!-- Mandatory identifier of the Tango device -->
    <uri>${ADDRESS}</uri>

    <!-- Mandatory channel which contains the status -->
    <channel name="state" type="tango" call="State" polling="1000" />

    <!-- Mandatory channel which contains the position -->
    <channel name="position" type="tango" call="${POSITION_ATTRIBUTE}" polling="1000" />

    <!-- Optional command which allow to abort the last move request -->
    <!--command name="abort" type="tango" call="TheCommandToAbort" /-->

</object>
"""

SPEC_TEMPLATE = """
<object class="SpecMotor">
  <data name="spec" uri="${ADDRESS}"/>
  <data name="spec" motor="${NAME}"/>
  <data name="spec" unit="${UNIT}"/>
</object>
"""


class MyMainWindow(Qt.QMainWindow):

    def __init__(self, *args, **kwargs):
        Qt.QMainWindow.__init__(self, *args, **kwargs)

        self.motor = None

        self.central_widget = Qt.QWidget(self)
        self.state = Qt.QLabel("  ", self.central_widget)
        self.position_label = Qt.QLabel("Position: ", self.central_widget)
        self.limits_label = Qt.QLabel("Position: ", self.central_widget)
        self.txt_move = Qt.QLineEdit(self.central_widget)
        self.unit_label = Qt.QLabel(" ", self.central_widget)

        layout = Qt.QHBoxLayout(self.central_widget)
        layout.addWidget(self.state)
        layout.addWidget(self.position_label)
        layout.addWidget(self.txt_move)
        layout.addWidget(self.unit_label)
        layout.addWidget(self.limits_label)
        self.setCentralWidget(self.central_widget)
        self.position_label.setSizePolicy(Qt.QSizePolicy.MinimumExpanding, Qt.QSizePolicy.Fixed)

        self.txt_move.returnPressed.connect(self.move)

    def set_motor(self, motor):
        self.motor = motor
        # connect motor object with Qt label
        motor.connect("positionChanged", self.motor_position_changed)
        motor.connect("stateChanged", self.motor_state_changed)
        motor.connect("limitsChanged", self.motor_limits_changed)

    def motor_position_changed(self, pos):
        self.position_label.setText("Position: %s" % pos)
        self.unit_label.setText("%s" % pos.units)
        self.txt_move.setText("%3.2f" % pos.magnitude)

    colors = {
        "MOVING": "blue",
        "ALARM": "yellow",
        "UNKNOWN": "grey",
        "ON": "green",
        "FAULT": "red",
        "DISABLED": "magenta"
    }

    def motor_state_changed(self, state):
        color = MyMainWindow.colors.get(state, "black")
        self.state.setStyleSheet("background-color: %s" % color)
        self.state.setText(state)

    def motor_limits_changed(self, limits):
        self.limits_label.setText("[%s ; %s]" % limits)

    def move(self):
        pos = str(self.txt_move.text())
        pos = cool.unit_registry.Quantity(pos, self.motor.position().units)
        self.motor.move(pos)


if __name__ == '__main__':

    # create the spec motor object from xml file
    if len(sys.argv) <= 1:
        print "Usage: %s tango DEVICE_TANGO_ADDRESS [POSITION_ATTRIBUT_NAME]" % sys.argv[0]
        print "Usage: %s spec SPEC_ADDRESS MOTOR_NAME UNIT" % sys.argv[0]
        sys.exit(-1)

    type_ = sys.argv[1]
    if type_ == "tango":
        config = TANGO_TEMPLATE
        position = "position"
        if len(sys.argv) == 3:
            position = sys.argv[2]
        config = config.replace("${ADDRESS}", sys.argv[2])
        config = config.replace("${POSITION_ATTRIBUTE}", sys.argv[3])
    elif type_ == "spec":
        config = SPEC_TEMPLATE
        config = config.replace("${ADDRESS}", sys.argv[2])
        config = config.replace("${NAME}", sys.argv[3])
        config = config.replace("${UNIT}", sys.argv[4])
    else:
        raise Exception("Type %s unexpected", type_)

    app = Qt.QApplication([])

    # make gevent loop working with Qt
    cool.init_qt()

    mainwin = MyMainWindow()
    mainwin.show()

    motor = cool.get_fromstring(config)
    mainwin.set_motor(motor)

    app.exec_()

from distutils.core import setup

package_data = {
    # Resources files for silx
    'cool.test': [
        'resources/*.xml',
        'resources/relative-dir/*.xml',
    ],
}


setup(name="cool", version="1.2.1",
      description="COntrol Objects Library",
      author="M. Guijarro, V. Valls (ESRF)",
      package_dir={"cool": "cool"},
      packages=["cool", "cool.control_objects", "cool.test"],
      install_requires=["gevent==1.2.*", "lxml", "pydispatcher"],
      package_data=package_data)

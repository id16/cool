import os
import sys
import gevent
from gevent import monkey; monkey.patch_all(thread=False, subprocess=False)
import pint
import logging
logging.getLogger(__name__).addHandler(logging.NullHandler())

from .core import get, get_fromstring

TIMER = None


unit_registry = pint.UnitRegistry()


def run_gevent():
    gevent.wait(timeout=0.02)


def init_qt():
    try:
        from PyQt5 import QtCore
    except ImportError:
        from PyQt4 import QtCore

    QtCore.pyqtRemoveInputHook()

    global TIMER
    if TIMER:
        return
    TIMER = QtCore.QTimer()
    TIMER.timeout.connect(run_gevent)
    TIMER.start(10)


def get_async_watcher():
    loop = gevent.get_hub().loop
    try:
        return loop.async_()
    except AttributeError:
        # As async is reserved in 3.7
        return getattr(loop, "async")()

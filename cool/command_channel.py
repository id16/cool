from .dispatcher import dispatcher
import gevent


class Command(object):
    def __new__(cls, *args, **kwargs):
        cls._initialized = False
        return object.__new__(cls)

    def __getattribute__(self, attr_name):
        if attr_name in ("device", "execute", "ready"):
            if not self._initialized:
                self._initialized = True
                try:
                    self.init()
                except:
                    self._initialized = False
                    raise
        return object.__getattribute__(self, attr_name)

    def __init__(self, name, username=None, **kwargs):
        self._name = name
        self._username = username
        self._attributes = kwargs
        self.__ready = False
        self.__timeout_tasks = dict()

    def init(self):
        pass

    def __str__(self):
        return "<command `%s' (%s)>" % (self.username(), self.name())

    def __call__(self, *args, **kwargs):
        return self.execute(*args, **kwargs)

    def hardware_connected(self, connected=None):
        if connected is None:
            self.__ready = True
            self.emit("__hw_connected__", True)
        else:
            self.__ready = connected
            self.emit("__hw_connected__", connected)

    def hardware_disconnected(self):
        self.__ready = False
        self.emit("__hw_connected__", False)

    def execute(self, *args, **kwargs):
        wait = kwargs.get("wait", True)
        timeout = kwargs.get("timeout", None)

        self.emit("command_started")
        task = gevent.spawn(self._execute, *args)
        task.link(self._emit_command_finished)

        if wait:
            return task.get(timeout=timeout)
        else:
            if timeout:
                self.__timeout_tasks[id(task)] = gevent.spawn_later(timeout, self._kill_task, timeout, task)
            return task

    def _execute(self, *args):
        raise NotImplementedError

    def _kill_task(self, seconds, task):
        task.kill(exception=gevent.Timeout(seconds))

    def _emit_command_finished(self, task):
        try:
            self.__timeout_tasks[id(task)].kill()
            del self.__timeout_tasks[id(task)]
        except KeyError:
            pass

        try:
            ret = task.get()
        except Exception as e:
            self.emit("command_failed", e)
        except gevent.Timeout as e:
            self.emit("command_failed", e)
        else:
            self.emit("command_finished", ret)

    def name(self):
        return self._name

    def username(self):
        return self._username or str(self.name())

    def connect(self, signal, callable_):
        dispatcher.connect(callable_, signal, self)

    def disconnect(self, signal, callable_):
        dispatcher.disconnect(callable_, signal, self)

    def emit(self, signal, *args):
        dispatcher.send(signal, self, *args)

    def abort(self):
        raise NotImplementedError

    def ready(self):
        return self.__ready


class Channel(object):
    def __new__(cls, *args, **kwargs):
        cls._initialized = False
        return object.__new__(cls)

    def __getattribute__(self, attr_name):
        if attr_name in ("value", "set_value", "connect", "ready"):
            if not self._initialized:
                self._initialized = True
                try:
                    self.init()
                except:
                    self._initialized = False
                    raise
        return object.__getattribute__(self, attr_name)

    def __init__(self, name, username=None, **kwargs):
        self._name = name
        self._username = username
        self._attributes = kwargs
        self.__ready = False

    def init(self):
        pass

    def __str__(self):
        return "<channel `%s' (%s)>" % (self.username(), self.name())

    def name(self):
        return self._name

    def username(self):
        return self._username or str(self.name())

    def hardware_connected(self, connected=None):
        if connected is None:
            self.__ready = True
            self.emit("__hw_connected__", True)
        else:
            self.__ready = connected
            self.emit("__hw_connected__", connected)

    def hardware_disconnected(self):
        self.__ready = False
        self.emit("__hw_connected__", False)

    def emit(self, signal, *args):
        dispatcher.send(signal, self, *args)

    def __emit_to_client(self, client_id, signal, *args):
        self.emit("update_%s" % client_id, signal, *args)

    def connect(self, signal, callable_):
        dispatcher.connect(callable_, signal, self)

    def disconnect(self, signal, callable_):
        dispatcher.disconnect(callable_, signal, self)

    def update(self, value, client_id=None):
        if True:  # client_id is None:
            self.emit("update", value)
        else:
            self.__emit_to_client(client_id, "update", value)

    def update_config(self, value, client_id=None):
        self.emit("config", value)

    def error(self, value, client_id=None):
        self.emit("error", value)

    def ready(self):
        return self.__ready

    def value(self):
        raise NotImplementedError

    def config(self):
        raise NotImplementedError

    def set_value(self, val):
        raise NotImplementedError

    def force_update(self, client_id=None):
        if self.ready():
            self.update(self.value(), client_id)
            try:
                self.update_config(self.config(), client_id)
            except NotImplementedError:
                pass

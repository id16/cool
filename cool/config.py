import os
from lxml import etree
import six

XML_FILES_PATH = ""


def set_xml_files_path(path):
    global XML_FILES_PATH
    if os.path.exists(path):
        XML_FILES_PATH = path
    else:
        raise RuntimeError("Path `%s' does not exist, XML_FILES_PATH remains unmodified" % path)


def xmlfile_path(cobject_name):
    return os.path.join(XML_FILES_PATH, cobject_name + ".xml")


class Configuration:
    def __init__(self, parent_object_name=None, xmltree_or_file=None):
        self.parent_object_name = parent_object_name

        self.root_document = parent_object_name if parent_object_name is not None else None

        if xmltree_or_file is None:
            # create an empty config node
            xmltree_or_file = etree.fromstring("<object></object>")

        if isinstance(xmltree_or_file, six.string_types):
            # filename
            self.root = etree.parse(xmltree_or_file).getroot()
        else:
            if hasattr(xmltree_or_file, "getroot"):
                self.root = xmltree_or_file.getroot()
            else:
                self.root = xmltree_or_file
        self._xpath_evaluator = etree.XPathElementEvaluator(self.root)

    def __getitem__(self, xpath_query):
        return self.raw_xpath(xpath_query)

    def raw_xpath(self, xpath_query):
        return self._xpath_evaluator(xpath_query)

    def get_root_document(self):
        """Return the root document name if the object is a sub element of an xml file description. Else None"""
        return self.root_document

    def is_document(self):
        """True if the object is the root of the document"""
        return self.parent_object_name is not None

    def tree(self):
        return self.root

    def tostring(self):
        return etree.tostring(self.root, pretty_print=True)

    def save(self):
        if not self.is_document():
            raise Exception("Save is not available")
        xmlfile = open(xmlfile_path(self.parent_object_name), "wt")
        xmlfile.write(self.tostring())
        xmlfile.close()

    def inject(self, xpath_query, xml):
        """Append xml string at node position pointed to by xpath_query"""
        nodes = list(filter(None, [isinstance(x, self.root.__class__) and x or None for x in self[xpath_query]]))
        parent_node = nodes[0]
        xml_tree = etree.fromstring(xml)
        parent_node.append(xml_tree)

    def append_string(self, xml):
        """Util to append an XML string at the end of the config root"""
        node = etree.fromstring(xml)
        self.root.append(node)

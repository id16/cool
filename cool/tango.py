import sys
import gevent
import traceback
import Queue
import time
import logging
import PyTango
from PyTango.gevent import DeviceProxy

import cool
from .command_channel import Command
from .command_channel import Channel
from . import Poller
from .dispatcher import saferef


_logger = logging.getLogger(__name__)


class TangoCommand(Command):
    def __init__(self, name, command, tango_device_name, username=None, **kwargs):
        Command.__init__(self, name, username, **kwargs)

        self.command = command
        self.device = None
        self.tango_device_name = tango_device_name

    def init(self):
        try:
            self.device = DeviceProxy(self.tango_device_name)
        except:
            self.device = None
        else:
            self.device.set_source(PyTango.DevSource.DEV)

        self.hardware_connected(self.device is not None)

    def _execute(self, *args, **kwargs):
        if self.device is None:
            self.init()

        cmd = getattr(self.device, self.command)
        try:
            ret = cmd(*args)
        except Exception as e:
            _exception, _error_string, tb = sys.exc_info()
            full_tb = traceback.extract_tb(tb)
            exception_tb = "".join(traceback.format_list(full_tb))
            msg = "%s: an error occurred when calling Tango command %s:\n%s\nTraceback:\n%s"
            raise RuntimeError(msg % (self.name(), self.command, e, exception_tb))
        except PyTango.DevFailed as error_dict:
            msg = "%s: a Tango error occurred when calling Tango command %s:\n%s"
            raise RuntimeError(msg % (self.name(), self.command, error_dict))
        else:
            return ret


def get_events_from_queue():
    while True:
        try:
            callback_ref, value = TangoChannel.events_queue.get_nowait()
        except Queue.Empty:
            break
        callback = callback_ref()
        if callable(callback):
            gevent.spawn(callback, value)


class TangoChannel(Channel):
    events_queue = Queue.Queue()
    events_watcher = cool.get_async_watcher()
    events_watcher.start(get_events_from_queue)

    def __init__(self, name, attribute_name, tango_device_name, username=None, **kwargs):
        Channel.__init__(self, name, username, **kwargs)

        global in_use
        in_use = True

        self.attribute_name = attribute_name
        self.attribute = None
        self.previous_value = None
        self.tango_device_name = tango_device_name
        self.device = None

        try:
            self.polling = int(kwargs.get('polling'))
        except:
            self.polling = None

    def init_device(self):
        self.device = DeviceProxy(self.tango_device_name)
        self.device.ping()

    def init(self):
        self.init_poller = Poller.poll(self.init_device,
                                       polling_period=3000,
                                       value_changed_callback=self.continue_init,
                                       error_callback=self.init_poll_failed,
                                       start_delay=100)

    def init_poll_failed(self, e, poller_id):
        self.init_poller = self.init_poller.restart(3000)

    def continue_init(self, _):
        self.init_poller.stop()
        self.device.set_source(PyTango.DevSource.DEV)
        self.hardware_connected(True)

        polling_expected = isinstance(self.polling, int)
        
        # try to register event
        if not polling_expected:
            try:
                self.device.subscribe_event(self.attribute_name,
                                            PyTango.EventType.CHANGE_EVENT,
                                            self, [], False)
            except PyTango.DevFailed:
                polling_expected = True
                _exctype, value = sys.exc_info()[:2]
                # the first one is the right one (others are deeper causes)
                e = value[0]
                _logger.warning("%s on device %s" % (e.desc, self.tango_device_name))

        # try to register to configuration events
        if not polling_expected:
            try:
                # FIXME: This call looks to deadlock in some cases
                self.device.subscribe_event(self.attribute_name,
                                            PyTango.EventType.ATTR_CONF_EVENT,
                                            self, [], False)
            except PyTango.DevFailed as _e:
                _exctype, value = sys.exc_info()[:2]
                # the first one is the right one (others are deeper causes)
                e = value[0]
                _logger.warning("%s on device %s" % (e.desc, self.tango_device_name))

        if polling_expected:
            if not isinstance(self.polling, int):
                # default polling value
                _logger.warning("Force polling to 60s")
                self.polling = 60000

            Poller.poll(self.value,
                        polling_period=self.polling,
                        value_changed_callback=self.update,
                        error_callback=self.poll_failed)


    def poll_failed(self, e, poller_id):
        try:
            self.init_device()
        except:
            pass

        poller = Poller.get_poller(poller_id)
        if poller is not None:
            poller.restart(1000)

    def push_event(self, event):
        if isinstance(event, PyTango._PyTango.AttrConfEventData):
            value = event.attr_conf
            TangoChannel.events_queue.put((saferef.safe_ref(self.update_config), value))
            TangoChannel.events_watcher.send()
        else:
            if event.attr_value is None or event.err:
                return
            if event.attr_value.quality not in (PyTango.AttrQuality.ATTR_VALID,
                                                PyTango.AttrQuality.ATTR_CHANGING):
                return
            TangoChannel.events_queue.put((saferef.safe_ref(self.update), event.attr_value.value))
            TangoChannel.events_watcher.send()

    def _wait_init(self, timeout):
        t0 = time.time()
        while not self.init_poller.is_stopped():
            time.sleep(0.1)
            if time.time() - t0 > timeout:
                msg = "Timeout waiting Tango device '%s` to be connected"
                raise RuntimeError(msg % self.tango_device_name)

    def value(self):
        if not self.device:
            self._wait_init(1)

        attr_read = self.device.read_attribute(self.attribute_name)
        if attr_read is None:
            msg = "Attribute '%s' from Tango device '%s' is not available"
            raise Exception(msg % (self.attribute_name, self.tango_device_name))
        return attr_read.value

    def config(self):
        if not self.device:
            self._wait_init(1)

        return self.device.get_attribute_config(self.attribute_name)

    def set_config(self, config):
        self.device.set_attribute_config(config)

    def set_value(self, value):
        if not self.device:
            self._wait_init(1)

        self.device.write_attribute(self.attribute_name, value)

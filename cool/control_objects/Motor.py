from cool.core import CObject


class MotorState:
    ON = "ON"
    OFF = "OFF"
    MOVING = "MOVING"
    FAULT = "FAULT"
    ALARM = "ALARM"
    UNKNOWN = "UNKNOWN"
    DISABLED = "DISABLED"


class Motor(CObject):

    def move(self, pos):
        """Move the motor to a position

        :param pos: Requested new position
        :type pos: pint.unit.Quantity
        """
        raise NotImplementedError()

    def move_relative(self, rel_pos):
        """Move the motor to a relative position according to the current
        one

        :param pos: Requested relative movement
        :type rel_pos: pint.unit.Quantity
        """
        raise NotImplementedError()

    def abort(self):
        """Abort the current move request"""
        raise NotImplementedError()

    def off(self):
        """Turn off the motor"""
        raise NotImplementedError()

    def on(self):
        """Turn on the motor"""
        raise NotImplementedError()

    def position(self):
        """Return the current position

        :rtype: pint.unit.Quantity
        """
        raise NotImplementedError()

    def state(self):
        """Return the current state

        :rtype: MotorState
        """
        raise NotImplementedError()

    def is_moving(self):
        """Return true if the motor is moving"""
        raise NotImplementedError()

    def set_limits(self, limits):
        """Apply new limits to the motor from a min,max tuple

        :type limits: (pint.unit.Quantity, pint.unit.Quantity)
        """
        raise NotImplementedError()

    def limits(self):
        """Return the limits of the motor which is a tuple min,max

        :rtype: (pint.unit.Quantity, pint.unit.Quantity)
        """
        raise NotImplementedError()

"""ConstAttribute object providing write of a const value

```
<object role="quantity" class="ConstAttribute">
    <value quantity="10mm" />
</object>

<object role="integer" class="ConstAttribute">
    <value integer="10" />
</object>

<object role="float" class="ConstAttribute">
    <value float="10.50" />
</object>

<object role="string" class="ConstAttribute">
    <value string="foobar" />
</object>

<object role="boolean" class="ConstAttribute">
    <value boolean="true" />
</object>

<object role="none" class="ConstAttribute">
    <value none="true" />
</object>

<!-- Array of values -->
<object role="quantity-list" class="ConstAttribute">
    <value quantity="-20mm" />
    <value quantity="0mm" />
</object>

<object role="one-element-list" list="true" class="ConstAttribute">
    <value boolean="false" />
</object>

<object role="empty-list" class="ConstAttribute">
</object>
```

"""
import cool
from .Attribute import Attribute, AttributeState


class ConstAttribute(Attribute):

    def __init__(self, name=None, xml_tree=None):
        Attribute.__init__(self, name, xml_tree)

    def init(self):
        Attribute.init(self)

        values = self.config["value"]
        value = []
        for v in values:
            if "boolean" in v.attrib:
                v = v.attrib["boolean"].lower()
                if v == "true" or v == "1":
                    value.append(True)
                elif v == "false" or v == "0":
                    value.append(False)
                else:
                    ValueError("String '%s' is not expected as boolean value" % v)
            elif "none" in v.attrib:
                v = v.attrib["none"].lower()
                if v == "true" or v == "1":
                    value.append(None)
                else:
                    ValueError("String '%s' is not expected as boolean value" % v)
            elif "integer" in v.attrib:
                v = v.attrib["integer"]
                value.append(int(v))
            elif "float" in v.attrib:
                v = v.attrib["float"]
                value.append(float(v))
            elif "string" in v.attrib:
                v = v.attrib["string"]
                value.append(str(v))
            elif "quantity" in v.attrib:
                v = v.attrib["quantity"]
                value.append(cool.unit_registry.Quantity(str(v)))
            else:
                raise ValueError("ConstAttribute expected value, but noting found")

        v = self.config["@list"]
        if len(v) != 0:
            force_list = v[0].lower() in ["true", "1"]
        else:
            force_list = False

        if force_list:
            self.__value = value
        elif len(value) == 0:
            self.__value = value
        elif len(value) == 1:
            self.__value = value[0]
        else:
            self.__value = value

    # State

    def state(self):
        return AttributeState.READONLY

    # Value

    def set_value(self, value):
        raise Exception("ConstAttribute is not editable")

    def value(self):
        return self.__value

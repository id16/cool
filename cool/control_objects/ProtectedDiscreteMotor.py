#!/usr/bin/env python
# -*- coding:utf-8 -*-

"""ProtectedDiscreteMotor control object

<object class="ProtectedDiscreteMotor">

    <!-- Check the DiscreteMotor to have description of the properties -->

    <object href="name_of_the_xml_file" role="motor" />
    <position name="a" offset="0.5" />
    <position name="b" offset="0.8" delta="0.02" />
    <position name="c" offset="0.99" delta="0.02" />
    <position name="d" offset="3.1" delta="0.02" />
    <position name="e" offset="9.5" description="Gold coated Diamond filters" />

    <!-- A reference to a safety shutter. This shutter must be closed to be
        allowed to move the discrete motor -->

    <object href="name_of_the_xml_file" role="safety_shutter" />

</object>
"""

from .DiscreteMotor import DiscreteMotor
from .Motor import MotorState


class ProtectedDiscreteMotor(DiscreteMotor):

    def __init__(self, *args, **kwargs):
        """Constructor"""
        DiscreteMotor.__init__(self, *args, **kwargs)

    def init(self):
        """Initialize the component. Mostly read the configuration."""
        if "safety_shutter" in self.objects:
            self.__safety_shutter = self.objects["safety_shutter"]
        else:
            self.__safety_shutter = None
        DiscreteMotor.init(self)
        if self.__safety_shutter:
            self.__safety_shutter.connect("stateChanged", self.__shutter_state_changed)

    def __shutter_state_changed(self, state):
        self.update_state()

    def _safety_shutter(self):
        """Access to the safety shutter. Especially for tests"""
        return self.__safety_shutter

    def _is_move_available(self):
        if self.__safety_shutter is not None:
            # FIXME We should request the real state and not a cached one
            if not self.__safety_shutter.is_closed():
                return False
        return True

    def _compute_state(self):
        state = DiscreteMotor._compute_state(self)
        if state == MotorState.OFF:
            return state
        if not self._is_move_available():
            return MotorState.DISABLED
        return state

    def debug_move(self, position):
        """Allow to set a specific position of the motor"""

        if not self._is_move_available():
            raise Exception("Move to position aborted. Safety shutter is not closed")

        DiscreteMotor.debug_move(self, position)

    def move_position_id(self, position_id):
        """Move the motor to a named position."""

        if not self._is_move_available():
            raise Exception("Move to position aborted. Safety shutter is not closed")
        DiscreteMotor.move_position_id(self, position_id)

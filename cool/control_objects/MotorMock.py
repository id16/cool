"""MotorMock control object

<object class="MotorMock">
    <!-- position of the motor. It is mandatory -->
    <config name="initial_value" value="5mm" />

    <!-- Minimal position of the motor. It is mandatory -->
    <config name="min_value" value="0mm" />

    <!-- Maximal position of the motor. It is mandatory -->
    <config name="max_value" value="10mm" />

    <!-- Step used to move the motor. It is mandatory -->
    <config name="step" value="0.1mm" />

    <!-- Optional unit used in case initial_value/min/max/step do not provide
        it. -->
    <config name="unit" value="mm" />

    <!-- Delay between 2 step used. It is mandatory -->
    <config name="delay" value="0.1" />

    <!-- Assign an initial movement. It is optional -->
    <config name="requested_value" value="8mm" />
</object>
"""

import cool
from cool.core import CObject
from cool.task_utils import task
from cool.task_utils import cleanup
import time
import logging
import gevent.event
from .Motor import Motor
from .Motor import MotorState


class MotorMock(Motor):
    """Create a virtual spec motor.
    You can control the initial_value, the delay between update, the step,
    and an already requested__value (which means the motor is already moving)
    """
    def __init__(self, *args, **kwargs):
        CObject.__init__(self, *args, **kwargs)
        self.__thread = None
        self.__state = MotorState.FAULT
        self.__is_moving = gevent.event.Event()

    def init(self):

        # optional unit
        default_unit = None
        v = self.config["config[@name='unit']/@value"]
        if len(v) == 1:
            default_unit = cool.unit_registry.Quantity(v[0]).units()

        def string_to_quantity(string):
            v = cool.unit_registry.Quantity(string)
            if v.unitless:
                # use the configured unit by default
                v = cool.unit_registry.Quantity(v.magnitude, default_unit)
            return v

        # mandatory configuration
        v = self.config["config[@name='initial_value']/@value"][0]
        self.__value = string_to_quantity(v)
        v = self.config["config[@name='min_value']/@value"][0]
        self.__min_value = string_to_quantity(v)
        v = self.config["config[@name='max_value']/@value"][0]
        self.__max_value = string_to_quantity(v)
        assert(self.__min_value < self.__max_value)
        assert(self.__value >= self.__min_value)
        assert(self.__value <= self.__max_value)

        v = self.config["config[@name='step']/@value"][0]
        self.__step = string_to_quantity(v)
        assert(self.__step.magnitude > 0)

        v = self.config["config[@name='delay']/@value"][0]
        self.__delay = float(v)
        assert(self.__delay > 0)

        # optional configuration
        requested_value = None
        v = self.config["config[@name='requested_value']/@value"]
        if len(v) == 1:
            requested_value = string_to_quantity(v[0])

        logging.getLogger(__name__).debug("spec_motor_mock: value=%s, min=%s, max=%s, step=%s, delay=%s, requested_value=%s", self.__value, self.__min_value, self.__max_value, self.__step, self.__delay, requested_value)

        if requested_value is not None:
            self.move(requested_value)
        else:
            if self.__value == self.__min_value or self.__value == self.__max_value:
                self.__set_state(MotorState.ALARM)
            else:
                self.__set_state(MotorState.ON)

    def connect_notify(self, signal):
        if signal == "stateChanged":
            self.__state_changed(self.__state)
        elif signal == "positionChanged":
            self.__position_changed(self.__value)
        elif signal == "limitsChanged":
            self.__limits_changed(*self.limits())

    def move(self, pos):
        if self.state() == MotorState.DISABLED:
            raise Exception("Motor is disabled")
        if self.state() == MotorState.OFF:
            raise Exception("Motor is off")

        if isinstance(pos, (int, long, float)):
            pos = cool.unit_registry.Quantity(pos)
        if self.__value.dimensionality != pos.dimensionality:
            raise Exception("Incompatibility between %s and %s" % (self.__value.units, pos.units))
        self.__stop_thread()
        self.__requested_value = pos.to(self.__value.units)
        self.__start_thread()

    def move_relative(self, rel_pos):
        self.move(self.__value + rel_pos)

    def is_moving(self):
        return self.__is_moving.is_set()

    def abort(self):
        self.__stop_thread()

    def on(self):
        if self.state() == MotorState.OFF:
            self.__set_state(MotorState.ON)

    def off(self):
        self.__stop_thread()
        self.__set_state(MotorState.OFF)

    def __set_state(self, state):
        if self.__state == state:
            return
        self.__state = state
        self.__state_changed(state)

    def _force_disabled(self):
        """Force the motor to be disabled.

        It should only be used for testing device
        """
        self.__set_state(MotorState.DISABLED)

    def _force_fault(self):
        """Force the motor to be in fault.

        It should only be used for testing device
        """
        self.__set_state(MotorState.FAULT)

    def __set_value(self, value):
        if self.__value == value:
            return
        self.__value = value
        self.__position_changed(value)

    def __state_changed(self, state):
        self.emit("stateChanged", state)

    def __position_changed(self, position):
        self.emit("positionChanged", position)

    def __limits_changed(self, min_value=None, max_value=None):
        if min_value is None and max_value is None:
            min_value, max_value = self.limits()
        self.emit("limitsChanged", (min_value, max_value))

    def position(self):
        return self.__value

    def state(self):
        return self.__state

    def set_limits(self, limits):
        self.__min_value, self.__max_value = limits
        self.__limits_changed()

    def limits(self):
        return (self.__min_value, self.__max_value)

    def __start_thread(self):
        self.__thread = self.__loop_thread(wait=False)
        self.__is_moving.wait()

    def __stop_thread(self):
        if self.__thread:
            self.__thread.kill()

    @task
    def __loop_thread(self):
        final_state = MotorState.ON

        def set_final_state():
            self.__thread = None
            self.__set_state(final_state)
            self.__is_moving.clear()

        with cleanup(set_final_state):
            self.__set_state(MotorState.MOVING)
            self.__is_moving.set()

            while True:
                value = self.__value

                if abs(value - self.__requested_value) <= self.__step:
                    # set the final requested value
                    self.__set_value(self.__requested_value)
                    break

                # compute new value
                if value < self.__requested_value:
                    delta = self.__step
                else:
                    delta = -self.__step
                value = value + delta

                # clamp min/max
                if value <= self.__min_value:
                    self.__set_value(self.__min_value)
                    final_state = MotorState.ALARM
                    break
                if value >= self.__max_value:
                    self.__set_value(self.__max_value)
                    final_state = MotorState.ALARM
                    break

                self.__set_value(value)

                time.sleep(self.__delay)

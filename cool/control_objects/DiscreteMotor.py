#!/usr/bin/env python
# -*- coding:utf-8 -*-

"""DiscreteMotor control object

Behaviour:

The available state are the same as a Motor. Only the meaning of ALARM have
changed.

- ON : The motor has stopped and is ready to accept new requests
- MOVING : The motor is moving to a new position
- FAULT : There is a problem
- ALARM : The motor is ON but reach a limit, or is not on a naming position

<object class="DiscreteMotor">

    <!-- Name of the XML file (without the extension) containing
        description of the SpecMotor controlled -->
    <object href="name_of_the_xml_file" role="motor" />

    <!-- Set of positions identified by a name and an offset -->
    <position name="a" offset="0.5mm" />

    <!-- we can specify a delta in witch the position of the motor is still
        considered to be inside the named position.
        here, while the motor position is between 0.78 and 0.82 the discrete
        motor returns "b" as named position -->
    <position name="b" offset="0.8mm" delta="0.02mm" />

    <!-- ... -->
    <position name="c" offset="0.99mm" delta="0.02mm" />
    <position name="d" offset="3.1mm" delta="0.02mm" />

    <!-- You can provide a human readable description -->
    <position name="e" offset="9.5mm" description="Gold coated Diamond filters" />

    <!-- BTW you should avoid overlap of locations -->

</object>
"""

import cool
from cool.core import CObject
import logging
from .Motor import Motor
from .Motor import MotorState


class Position:
    """Store information about a position.
    It uses Old-style class to easily allow to add attributes"""
    def __init__(self):
        self.name = None
        self.offset = None
        self.delta = None
        self.description = None

    def __str__(self):
        return "(name: %s, offset: %s)" % (self.name, self.offset)


class DiscreteMotor(CObject):

    def __init__(self, *args, **kwargs):
        """Constructor"""
        CObject.__init__(self, *args, **kwargs)

    def init(self):
        """Initialize the component. Mostly read the configuration."""
        if "motor" not in self.objects:
            raise Exception("A motor object is expected")
        self.__motor = self.objects["motor"]
        if not isinstance(self.__motor, Motor):
            raise Exception("Object 'motor' must be an AbstractMotor object")

        log = logging.getLogger(__name__)
        log.debug("discrete motor:")

        positions = self.config["position"]
        self.__configuration = {}
        for element in positions:
            location = self._read_position_node(element)

            # TODO maybe we also should add a test for offset outside motor
            # limits

            if location.name in self.__configuration:
                raise Exception("duplicated position name '%s'" % location.name)

            self.__configuration[location.name] = location
            log.debug("discrete motor: position %s %s %s", location.name, location.offset, location.delta)

        self.__position_id = None
        self.__unreachable_position_ids = []
        self.__state = MotorState.UNKNOWN
        self.__motor_state = MotorState.UNKNOWN

        self.__motor.connect("stateChanged", self.__motor_state_changed)
        self.__motor.connect("positionChanged", self.__motor_position_changed)
        self.__motor.connect("limitsChanged", self.__motor_limits_changed)

    def _read_position_node(self, node):
        name = node.get("name")
        offset = node.get("offset")
        description = node.get("description")
        if name is None:
            raise Exception("name attribute expected for position")
        if offset is None:
            raise Exception("name attribute expected for position")
        delta = node.get("delta")

        offset = cool.unit_registry.Quantity(offset)
        if delta is not None:
            delta = cool.unit_registry.Quantity(delta)

        result = Position()
        result.name = name
        result.offset = offset
        result.delta = delta
        result.description = description
        return result

    def store(self):
        CObject.store(self)

        previous_names = self.config["position/@name"]
        current_names = self.__configuration.keys()

        # remove deleted ones
        need_deleting = set(previous_names) - set(current_names)
        for name in need_deleting:
            node = self.config["position[@name='%s']" % name][0]
            node.getparent().remove(node)

        # create new nodes
        need_creation = set(current_names) - set(previous_names)
        for name in need_creation:
            pos = self.__configuration[name]
            node = self.config.root.makeelement("position")
            self._write_position_node(node, pos)
            self.config.root.append(node)

        # update in case there is changes
        others = set(current_names) & set(previous_names)
        for name in others:
            node = self.config["position[@name='%s']" % name][0]
            pos = self.__configuration[name]
            self._write_position_node(node, pos)

    def _write_position_node(self, node, position):
        node.set("name", position.name)
        node.set("offset", str(position.offset))
        if position.delta:
            node.set("delta", str(position.delta))
        if position.description:
            node.set("description", position.description)

    def debug_position(self):
        """Get the real internal position of the motor"""
        return self.__motor.position()

    def debug_move(self, position):
        """Allow to set a specific position of the motor"""
        return self.__motor.move(position)

    def connect_notify(self, signal):
        if signal == "positionIdChanged":
            self.__fire_position_id_changed(self.__position_id)
        elif signal == "positionChanged":
            self.__motor.connect_notify(signal)
        elif signal == "stateChanged":
            self.__fire_state_changed(self.__state)
        elif signal == "configurationChanged":
            self.__fire_configuration_changed(self.get_configuration())
        elif signal == "unreachablePositionIdsChanged":
            self.__fire_unreachable_position_ids_changed()
        else:
            raise Exception("%s is not an expected event" % signal)

    def abort(self):
        """Abort the current move request"""
        self.__motor.abort()

    def on(self):
        """Turn on the motor"""
        self.__motor.on()

    def off(self):
        """Turn off the motor"""
        self.__motor.off()

    def position_id(self):
        """Return the current named position of the motor, else None if the
        motor is not as an known position."""
        return self.__position_id

    def move_position_id(self, position_id):
        """Move the motor to a named position."""
        self.__move_position_id(position_id)

    def __set_position_id(self, position_id):
        """Set position_id attribute and emit positionIdChanged,
        if it is needed"""
        if position_id == self.__position_id:
            return

        self.__position_id = position_id
        self.__fire_position_id_changed(position_id)
        state = self._compute_state()
        self.__set_state(state)

    def __fire_position_id_changed(self, position_id):
        """Emit positionIdChanged"""
        self.emit("positionIdChanged", position_id)

    def __fire_configuration_changed(self, configuration):
        """Emit configurationChanged"""
        self.emit("configurationChanged", configuration)

    def state(self):
        """
        Get the current state of the device

        :returns: the current state
        :rtype: str
        """
        return self.__state

    def __motor_state_changed(self, state):
        if self.__motor_state == state:
            return
        self. __motor_state = state
        self.update_state()

    def update_state(self):
        state = self._compute_state()
        self.__set_state(state)

    def _compute_state(self):
        """
        Compute the final state of the device.

        The method only compute the expected current state and do not apply it.

        :returns: the current expected state
        :rtype: str
        """
        state = self. __motor_state
        if self. __motor_state == MotorState.ON and self.__position_id is None:
            # motor do not move outside a known location
            state = MotorState.ALARM
        elif self. __motor_state == MotorState.ALARM and self.__position_id is not None:
            # motor is at a known position on a limit
            state = MotorState.ON

        return state

    def __set_state(self, state):
        if self.__state == state:
            return
        self.__state = state
        self.__fire_state_changed(state)

    def __fire_state_changed(self, state):
        self.emit("stateChanged", state)

    def create_position(self, name, offset, delta=None, description=None):
        """Factory to create a position object"""
        p = Position()
        p.name = name
        p.offset = offset
        p.delta = delta
        p.description = description
        return p

    def __is_position(self, position):
        return hasattr(position, 'name') and hasattr(position, 'offset')

    def __normalize_position(self, position):
        if not hasattr(position, 'description'):
            position.description = None
        if not hasattr(position, 'delta'):
            position.delta = None
        return position

    def set_configuration(self, configuration):
        """Set a full configuration of the motor. Argument is a list of object
        with at least attribute name and offset and delta (see Position class)
        """
        new_conf = {}
        for p in configuration:
            if not self.__is_position(p):
                raise Exception("Configuration contains invalid position object")
            self.__normalize_position(p)
            new_conf[p.name] = p

        self.__configuration = new_conf
        configuration = self.__configuration.values()
        self.__fire_configuration_changed(configuration)
        # then position id and unreachable positions can change
        self.__update_reachable_position_ids()
        self.__motor_position_changed(self.__motor.position())

    def add_position_id(self, position):
        """Add or update a position according to the name. Argument is an object
        with at least attribute name and offset and delta (see Position class)
        """
        if not self.__is_position(position):
            raise Exception("Position is not a valid object")
        self.__normalize_position(position)
        new_conf = self.__configuration
        new_conf[position.name] = position
        self.set_configuration(new_conf.values())
        pass

    def remove_position_id(self, name):
        """Remove a position according to the name"""
        new_conf = self.__configuration
        del new_conf[name]
        self.set_configuration(new_conf.values())

    def get_configuration(self):
        """Return the internal configuration as a list of named positions"""
        return self.__configuration.values()

    def get_closest_location(self, requested_position, default_delta=None):
        """Get the id of the closest location, according to a requested
        position.
        Return None if no location is found or if the difference is bigger than
        the delta.

        TODO the closest one should be the right one.
        but here the first one is the right one.
        It works while there is no overlap of locations
        """

        if default_delta is None:
            unit = self.__motor.position().units
            default_delta = cool.unit_registry.Quantity(0.00001, unit)

        for p in self.__configuration.values():
            pos, delta = p.offset, p.delta
            if delta is None:
                delta = default_delta

            if pos - delta < requested_position and requested_position < pos + delta:
                return p.name

        return None

    def __motor_position_changed(self, current_position):
        """Callback method when the real motor position change"""
        self.emit("positionChanged", current_position)
        position_id = self.get_closest_location(current_position)
        self.__set_position_id(position_id)

    def __motor_limits_changed(self, limits):
        self.__update_reachable_position_ids()

    def unreachable_position_ids(self):
        return self.__unreachable_position_ids

    def __update_reachable_position_ids(self):
        if self.__motor_state == MotorState.UNKNOWN:
            return
        limits = self.__motor.limits()
        unreachable = []
        for position in self.__configuration.values():
            if limits[0] <= position.offset <= limits[1]:
                pass
            else:
                unreachable.append(position.name)

        if self.__unreachable_position_ids != unreachable:
            self.__unreachable_position_ids = unreachable
            self.__fire_unreachable_position_ids_changed()

    def __fire_unreachable_position_ids_changed(self, ):
        self.emit("unreachablePositionIdsChanged", self.__unreachable_position_ids)

    def __move_position_id(self, position_id):
        """Move the motor at a named position"""
        if position_id is None:
            raise Exception("position_id expected but None found")

        if position_id not in self.__configuration:
            raise Exception("Position id '%s' is not available" % position_id)

        data = self.__configuration[position_id]
        position = data.offset
        self.__motor.move(position)

"""Spec motor control object

<object class="EsrfSpecMotor">
  <data name="spec" uri="nano:valentin"/>
  <data name="spec" motor="sy"/>
</object>
"""

from .Motor import MotorState
from .SpecMotor import SpecMotor
from . import EsrfSpecLockManager


class EsrfSpecMotor(SpecMotor):
    """Spec motor which use specific ESRF Spec environment to provide disabled
    state (which is not provided by Spec by default)."""

    def __init__(self, *args, **kwargs):
        SpecMotor.__init__(self, *args, **kwargs)

    def init(self):
        self.__locked = False
        SpecMotor.init(self)
        self.__lock_manager = EsrfSpecLockManager.get_instance(self.spec_name)
        self.__lock_manager.connect("motor_%s_changed" % self.motor_name, self.__lock_changed)

    def __lock_changed(self, locked):
        """Callback when motor lock change"""
        if self.__locked == locked:
            return
        self.__locked = locked
        self._update_state()

    def _compute_state(self):
        """Computes and returns the expected status of the device."""
        state = SpecMotor._compute_state(self)
        if state == MotorState.ALARM or MotorState.ON or MotorState.UNKNOWN:
            if self.__locked:
                state = MotorState.DISABLED
        return state

"""Boolean attribute provided by a non boolean attribute, usually a int or a string.


<object class="BooleanAttribute">
    <object role="foo" class="AttributeMock">
        <value string="1" />
    </object>
</object>

<object class="BooleanAttribute">
    <object role="foo" class="AttributeMock">
        <value string="1" />
    </object>
    <config inverted="true" />
</object>

"""
from .Attribute import Attribute
from .Attribute import AttributeState
from .Attribute import AttributeValueNotAvailable

class _BooleanAttributeCodec:
    INT = 0
    INT_AS_STRING = 1
    STRING = 2


class BooleanAttribute(Attribute):

    def __init__(self, name=None, xml_tree=None, attribute=None, inverted=None, codec=None):
        Attribute.__init__(self, name, xml_tree)
        self.__inverted = inverted
        self.__attribute = attribute
        self.__codec = codec
        if attribute is not None:
            self.init()

    def init(self):
        Attribute.init(self)

        if self.__inverted is None:
            v = self.config["config/@inverted"]
            if len(v) != 0:
                self.__inverted = v[0] in ["true", "1"]

        if self.__attribute is None:
            self.__attribute = self.objects.itervalues().next()
        self.__attribute.connect("stateChanged", self.__attribute_state_changed)
        self.__attribute.connect("valueChanged", self.__attribute_value_changed)

    # State

    def state(self):
        return self.__attribute.state()

    def __attribute_state_changed(self, state_):
        self._fire_state_changed()

    # Value

    def __attribute_value_changed(self, value_):
        self._fire_value_changed()

    def __update_codec(self):
        if self.__codec == None:
            value = self.__attribute.value()
            if value in set(["0", "1"]):
                self.__codec = _BooleanAttributeCodec.INT_AS_STRING
            elif value in set([0, 1]):
                self.__codec = _BooleanAttributeCodec.INT
            elif value in set(["true", "false"]):
                self.__codec = _BooleanAttributeCodec.STRING

    def __convert_to_sub_attribute(self, value):
        self.__update_codec()
        if self.__codec == _BooleanAttributeCodec.INT:
            return 1 if value else 0
        if self.__codec == _BooleanAttributeCodec.STRING:
            return "true" if value else "false"
        if self.__codec == _BooleanAttributeCodec.INT_AS_STRING:
            return "1" if value else "0"

        raise Exception("Unsupported boolean codec %s" % self.__codec)

    def __convert_from_sub_attribute(self, value):
        if value in set(["0", 0, "false"]):
            return False
        if value in set(["1", 1, "true"]):
            return True

        raise Exception("Unexpected boolean value '%s'" % value)

    def set_value(self, value):
        if self.__inverted:
            value = not value
        value = self.__convert_to_sub_attribute(value)
        self.__attribute.set_value(value)

    def value(self):
        if self.__attribute.state() == AttributeState.UNKNOWN:
            raise AttributeValueNotAvailable()
        value = self.__attribute.value()
        value = self.__convert_from_sub_attribute(value)
        if self.__inverted:
            value = not value
        return value

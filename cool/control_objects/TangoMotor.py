"""Tango motor control object


<object class = "TangoMotor">
    <!-- Mandatory identifier of the Tango device -->
    <uri>d29/bsh/1</uri>

    <!-- Mandatory channel which contains the status -->
    <channel name="state" type="tango" call="State" />

    <!-- Mandatory channel which contains the position -->
    <channel name="position" type="tango" call="TheAttributeWithPosition" />

    <!-- Optional command which allow to abort the last move request -->
    <command name="abort" type="tango" call="TheCommandToAbort" />
    <!-- Optional command which allow to turn on the device -->
    <command name="on" type="tango" call="TheCommandToTurnOn" />
    <!-- Optional command which allow to turn off the device -->
    <command name="off" type="tango" call="TheCommandToTurnOff" />
</object>
"""

import cool
from cool.core import CObject
from .Motor import MotorState
from .AbstractMotor import AbstractMotor


_TANGO_TO_STATE = {
    "ON": MotorState.ON,
    "OFF": MotorState.OFF,
    "CLOSE": MotorState.UNKNOWN,
    "OPEN": MotorState.UNKNOWN,
    "INSERT": MotorState.UNKNOWN,
    "EXTRACT": MotorState.UNKNOWN,
    "MOVING": MotorState.MOVING,
    "STANDBY": MotorState.UNKNOWN,
    "FAULT": MotorState.FAULT,
    "INIT": MotorState.UNKNOWN,
    "RUNNING": MotorState.UNKNOWN,
    "ALARM": MotorState.ALARM,
    "DISABLE": MotorState.DISABLED,
    "UNKNOWN": MotorState.UNKNOWN,
}


class TangoMotor(AbstractMotor):

    def __init__(self, *args, **kwargs):
        CObject.__init__(self, *args, **kwargs)

    def init(self):
        if "state" not in self.channels:
            raise Exception("A state channel is expected.")
        if "position" not in self.channels:
            raise Exception("A position channel is expected.")

        self.__state = self.channels["state"]
        self.__position = self.channels["position"]
        self.__abort_supported = 'abort' in self.commands
        self.__on_supported = 'on' in self.commands
        self.__off_supported = 'off' in self.commands

        self.__unit = None
        self.__previous_limits = None

        self.channels["state"].connect("update", self.__state_changed)
        self.channels["position"].connect("update", self.__position_changed)
        self.channels["position"].connect("config", self.__position_config_changed)

        unit = self.__position.config().unit
        self.__unit = cool.unit_registry.Quantity(unit).units
        self.__previous_limits = self.limits()

    def connect_notify(self, signal):
        if signal == "stateChanged":
            self.__fire_state_changed(self.state())
        elif signal == "positionChanged":
            self.__fire_position_changed(self.position())
        elif signal == "limitsChanged":
            self.__fire_limits_changed(self.limits())
        else:
            raise Exception("Unsupported signal %s" % signal)

    def _hw_move(self, pos):
        return self.__position.set_value(pos)

    def _hw_move_relative(self, rel_pos):
        pos = self.position() + rel_pos
        return self.__position.set_value(pos)

    def abort(self):
        if not self.__abort_supported:
            raise Exception("Abort is not supported. Add an abort command to your configuration.")
        self.commands["abort"]()

    def on(self):
        if not self.__on_supported:
            raise Exception("Turning on the device is not supported. Add an on command to your configuration.")
        self.commands["on"]()

    def off(self):
        if not self.__off_supported:
            raise Exception("Turning off the device is not supported. Add an off command to your configuration.")
        self.commands["off"]()

    def _hw_position(self):
        return self.__position.value()

    def _position_unit(self):
        return self.__unit

    def __state_changed(self, tango_state):
        self.__fire_state_changed(self.state(tango_state))

    def __position_changed(self, pos):
        pos = self._hw_to_position(pos)
        self.__fire_position_changed(pos)

    def __fire_position_changed(self, pos):
        self.emit("positionChanged", pos)

    def state(self, tango_state=None):
        if tango_state is None:
            tango_state = self.__state.value()
        state = _TANGO_TO_STATE[str(tango_state)]
        return state

    def is_moving(self):
        return self.state() == MotorState.MOVING

    def __fire_state_changed(self, state):
        self.emit("stateChanged", state)

    def _hw_set_limits(self, limits):
        config = self.__position.config()
        config.min_value = str(limits[0])
        config.max_value = str(limits[1])
        self.__position.set_config(config)

    def _hw_limits(self):
        config = self.__position.config()
        return (float(config.min_value), float(config.max_value))

    def __position_config_changed(self, value):
        # limit update
        limits = self.limits()
        if self.__previous_limits != limits:
            self.__previous_limits = limits
            self.__fire_limits_changed(limits)

        # unit update
        unit = value.unit
        unit = cool.unit_registry.Quantity(unit).units
        if self.__unit != unit:
            self.__unit = unit
            self.__fire_limits_changed(self.limits())
            self.__fire_position_changed(self.position())

    def __fire_limits_changed(self, limits):
        self.emit("limitsChanged", limits)

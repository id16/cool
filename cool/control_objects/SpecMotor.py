"""Spec motor control object

<object class="SpecMotor">
  <data name="spec" uri="blissdb:matias"/>
  <data name="spec" motor="msim"/>

  <!-- Optional unit -->
  <data name="spec" unit="mm">
</object>
"""

import cool
from cool.core import CObject
from SpecClient_gevent import SpecMotor as SM
import logging
import gevent

from .Motor import MotorState
from .AbstractMotor import AbstractMotor

spec2state = {
    SM.NOTINITIALIZED: MotorState.FAULT,
    SM.UNUSABLE: MotorState.FAULT,
    SM.READY: MotorState.ON,
    SM.MOVESTARTED: MotorState.MOVING,
    SM.MOVING: MotorState.MOVING,
    SM.ONLIMIT: MotorState.ALARM
}


class SpecMotor(AbstractMotor):

    def __init__(self, *args, **kwargs):
        CObject.__init__(self, *args, **kwargs)

    def init(self):
        self.__state = MotorState.UNKNOWN
        self.spec_name = self.config["data[@name='spec']/@uri"][0]
        self.motor_name = self.config["data[@name='spec']/@motor"][0]

        v = self.config["data[@name='spec']/@unit"]
        if len(v) != 0:
            self.unit = cool.unit_registry.Quantity(v[0]).units
        else:
            self.unit = cool.unit_registry.Quantity(0, "dimensionless").units

        self.__inisialized = gevent.event.Event()
        logging.getLogger(__name__).debug('spec_motor: spec_name="%s", motor_name="%s"', self.spec_name, self.motor_name)
        self._spec_motor = SM.SpecMotorA(self.motor_name, self.spec_name, {"motorPositionChanged": self.position_changed, "motorStateChanged": self.__state_changed, "motorLimitsChanged": self.limits_changed})
        self.__inisialized.set()

    def connect_notify(self, signal):
        if signal == "stateChanged":
            self.__fire_state_changed()
        elif signal == "positionChanged":
            self.__fire_position_changed(self.position())
        elif signal == "limitsChanged":
            self.limits_changed()

    def _hw_move(self, pos, **kwargs):
        self._spec_motor.move(pos, **kwargs)

    def _hw_move_relative(self, rel_pos, **kwargs):
        self._spec_motor.moveRelative(rel_pos, **kwargs)

    def _position_unit(self):
        return self.unit

    def abort(self):
        self._spec_motor.stop()

    def on(self):
        raise NotImplementedError("Spec do not provide an on/off state")

    def off(self):
        raise NotImplementedError("Spec do not provide an on/off state")

    def __state_changed(self, spec_state):
        """Callback when state of the sub system spec motor device changed.
        :param spec_state: State of the spec device
        :param spec_state: int
        """
        if self._spec_motor is None:
            self.__inisialized.wait()
        self._update_state()

    def _update_state(self):
        """Request a new computation of the state."""
        state = self._compute_state()
        self.__set_state(state)

    def _compute_state(self):
        """Compute the expected current state of the device.
        :rtype: MotorState
        """
        spec_state = self._spec_motor.getState()
        return spec2state[spec_state]

    def __set_state(self, state):
        """Set a new state to the device. It emit a 'stateChanged' event if the
        state changed.
        :param state: Expected state for the motor device
        :param state: MotorState
        """
        if self.__state == state:
            return
        self.__state = state
        self.__fire_state_changed()

    def __fire_state_changed(self):
        """Emit the current device state."""
        self.emit("stateChanged", self.__state)

    def position_changed(self, position):
        position = self._hw_to_position(position)
        self.__fire_position_changed(position)

    def __fire_position_changed(self, position):
        self.emit("positionChanged", position)

    def _hw_set_limits(self, limits):
        """FIXME: implement me please"""
        raise NotImplementedError()

    def limits_changed(self, min_value=None, max_value=None):
        if min_value is None and max_value is None:
            min_value, max_value = self.limits()
        else:
            min_value = self._hw_to_position(min_value)
            max_value = self._hw_to_position(max_value)
        self.emit("limitsChanged", (min_value, max_value))

    def _hw_position(self):
        return self._spec_motor.getPosition()

    def _hw_dial_position(self):
        return self._spec_motor.getDialPosition()

    def state(self):
        return self.__state

    def is_moving(self):
        return self.state() == MotorState.MOVING

    def sign(self):
        return self._spec_motor.getSign()

    def _hw_limits(self):
        return self._spec_motor.getLimits()

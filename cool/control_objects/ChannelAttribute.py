"""Attribute object providing read/write of a value


<object class="ChannelAttribute">
    <uri>d29/bsh/1</uri>
    <channel name="channel" type="tango" call="Position" />

    <!-- Use Tango unit metadata to create a quantity -->
    <config quantity="true" />
</object>


<object class="ChannelAttribute">
    <channel name="channel" call="XIA_ROI" uri="chico:eh_na" type="spec"/>

    <!-- Use this unit to create a quantity -->
    <config quantityUnit="mm" />
</object>

"""
import cool
from .Attribute import Attribute, AttributeState

# TODO Support tango read-only metadata
# TODO Support tango metadata event (for unit)


class ChannelAttribute(Attribute):

    def __init__(self, name=None, xml_tree=None, is_quantity=None, quantity_unit=None, channel=None):
        Attribute.__init__(self, name, xml_tree)
        self.__is_quantity = is_quantity
        self.__quantity_unit = quantity_unit
        self.__channel = channel
        if channel is not None:
            self.init()

    def init(self):
        Attribute.init(self)

        self.__value = None
        self.__state = AttributeState.UNKNOWN

        if self.__is_quantity is None:
            v = self.config["config/@quantity"]
            if len(v) != 0:
                self.__is_quantity = v[0] in ["true", "1"]
            else:
                self.__is_quantity = False

        if self.__quantity_unit is None:
            v = self.config["config/@quantityUnit"]
            if len(v) != 0:
                self.__quantity_unit = v[0]
                self.__is_quantity = True

        if self.__channel is None:
            self.__channel = self.channels.itervalues().next()
        self.__channel.connect("update", self.__value_changed)
        if self.__channel.ready():
            self.__value_changed(self.__channel.value())

    # State

    def state(self):
        return self.__state

    def __update_state(self):
        if self.__channel.ready():
            self.__set_state(AttributeState.ON)
        else:
            self.__set_state(AttributeState.UNKNOWN)

    def __set_state(self, state):
        if self.__state == state:
            return
        self.__state = state
        self._fire_state_changed()

    # Value

    def __normalize_device_value(self, value):
        if self.__quantity_unit is not None:
            if isinstance(value, basestring):
                value = float(value)
            return cool.unit_registry.Quantity(value, self.__quantity_unit)
        elif self.__is_quantity:
            if isinstance(value, basestring):
                value = float(value)
            unit = self.__channel.config().unit
            return cool.unit_registry.Quantity(value, unit)
        else:
            return value

    def __normalize_value_to_device(self, value):
        if self.__quantity_unit is not None:
            value = value.to(self.__quantity_unit).magnitude
        elif self.__is_quantity:
            unit = self.__channel.config().unit
            value = value.to(unit).magnitude

        if isinstance(self.__channel.value(), basestring):
            value = str(value)

        return value

    def __value_changed(self, value):
        value = self.__normalize_device_value(value)
        # the value is an internal object. In case of dict, value and __value is the same
        # if value == self.__value:
        #     return
        self.__value = value

        if self.state() == AttributeState.UNKNOWN:
            self.__update_state()

        self._fire_value_changed()

    def set_value(self, value):
        value = self.__normalize_value_to_device(value)
        self.__channel.set_value(value)

    def value(self):
        return self.__value

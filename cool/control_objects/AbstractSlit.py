from cool.core import CObject


class SlitState:
    # The device is ready
    ON = "ON"
    # The device is moving
    MOVING = "MOVING"
    # The device is in fault (no access to the motors...)
    FAULT = "FAULT"


class AbstractSlit(CObject):
    """Slit interface"""

    def state(self):
        """State of the device"""
        raise NotImplementedError()

    def offset(self):
        """Position of the gap

        :rtype: pint.unit.Quantity
        """
        raise NotImplementedError()

    def set_offset(self, position):
        """Move the blade to this new offset

        :type position: pint.unit.Quantity
        """
        raise NotImplementedError()

    def gap(self):
        """Size of the gap

        :rtype: pint.unit.Quantity
        """
        raise NotImplementedError()

    def set_gap(self, size):
        """Move the blades to this new gap size.

        :type size: pint.unit.Quantity
        """
        raise NotImplementedError()

    def position_blade1(self):
        """Position of the first blade.

        :rtype: pint.unit.Quantity
        """
        raise NotImplementedError()

    def move_blade1(self, position):
        """Move the first blade. A direct access to the motor while change
        both the gap and the offset.

        :type position: pint.unit.Quantity
        """
        raise NotImplementedError()

    def position_blade2(self):
        """Position of the second blade.

        :rtype: pint.unit.Quantity
        """
        raise NotImplementedError()

    def move_blade2(self, position):
        """Move the second blade. A direct access to the motor while change
        both the gap and the offset.

        :type position: pint.unit.Quantity
        """
        raise NotImplementedError()

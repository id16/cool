"""Shutter control object

<object class="Slit">
    <!-- It must be a Motor -->
    <object href="m1" role="blade1"/>

    <!-- It must be a Motor -->
    <object href="m2" role="blade2"/>

    <!-- It must be a Motor -->
    <object href="m3" role="offset"/>

    <!-- It must be a Motor -->
    <object href="m4" role="gap"/>
</object>
"""
from cool.core import CObject
from .AbstractSlit import AbstractSlit
from .AbstractSlit import SlitState
from .Motor import Motor
from .Motor import MotorState


class Slit(AbstractSlit):
    """Slit wrapper over an existing slit (for example a slit managed by spec).
    gap, offset, and the two blades are considered as distinct motors.
    """

    def __init__(self, *args, **kwargs):
        CObject.__init__(self, *args, **kwargs)

    def init(self):
        expected_motors = ["blade1", "blade2", "gap", "offset"]
        for name in expected_motors:
            if name not in self.objects:
                raise Exception("Object '%s' is expected" % name)

            motor = self.objects[name]
            if not isinstance(motor, Motor):
                raise Exception("Object '%s' must be a Motor object" % name)

        # connect to the positions
        self.objects["gap"].connect("positionChanged", self.__fire_gap_changed)
        self.objects["offset"].connect("positionChanged", self.__fire_offset_changed)
        self.objects["blade1"].connect("positionChanged", self.__fire_blade1_changed)
        self.objects["blade2"].connect("positionChanged", self.__fire_blade2_changed)

        # connect to the states
        self.__state = None
        self.blade1_state = self.objects["blade1"].state()
        self.blade2_state = self.objects["blade2"].state()
        self.objects["blade1"].connect("stateChanged", self.__on_blade1_state_changed)
        self.objects["blade2"].connect("stateChanged", self.__on_blade2_state_changed)
        self.__update_state()

    def connect_notify(self, signal):
        if signal == "stateChanged":
            self.__fire_state_changed(self.state())
        elif signal == "statusChanged":
            # FIXME do it
            pass
        elif signal == "gapChanged":
            self.objects["gap"].connect_notify("positionChanged")
        elif signal == "offsetChanged":
            self.objects["offset"].connect_notify("positionChanged")
        elif signal == "blade1Changed":
            self.objects["blade1"].connect_notify("positionChanged")
        elif signal == "blade2Changed":
            self.objects["blade2"].connect_notify("positionChanged")
        else:
            raise Exception("Unsupported signal %s" % signal)

    # State

    def state(self):
        return self.__state

    def __set_state(self, state):
        if self.__state == state:
            return
        self.__state = state
        self.__fire_state_changed(state)

    def __fire_state_changed(self, state):
        self.emit("stateChanged", state)

    def __on_blade1_state_changed(self, state):
        if self.blade1_state == state:
            return
        self.blade1_state = state
        self.__update_state()

    def __on_blade2_state_changed(self, state):
        if self.blade2_state == state:
            return
        self.blade2_state = state
        self.__update_state()

    def __update_state(self):
        if self.blade1_state == MotorState.FAULT or self.blade2_state == MotorState.FAULT:
            state = SlitState.FAULT
        elif self.blade1_state == MotorState.MOVING or self.blade2_state == MotorState.MOVING:
            state = SlitState.MOVING
        else:
            state = SlitState.ON
        self.__set_state(state)

    # Offset

    def offset(self):
        return self.objects["offset"].position()

    def set_offset(self, position):
        self.objects["offset"].move(position)

    def __fire_offset_changed(self, position):
        self.emit("offsetChanged", position)

    # Gap

    def gap(self):
        return self.objects["gap"].position()

    def set_gap(self, size):
        self.objects["gap"].move(size)

    def __fire_gap_changed(self, position):
        self.emit("gapChanged", position)

    # Motor 1

    def position_blade1(self):
        return self.objects["blade1"].position()

    def move_blade1(self, position):
        self.objects["blade1"].move(position)

    def __fire_blade1_changed(self, position):
        self.emit("blade1Changed", position)

    # Motor 2

    def position_blade2(self):
        return self.objects["blade2"].position()

    def move_blade2(self, position):
        self.objects["blade2"].move(position)

    def __fire_blade2_changed(self, position):
        self.emit("blade2Changed", position)

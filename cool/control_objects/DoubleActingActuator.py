"""Double acting actuator control object

To move this actuator it is needed to do an action in 2 attributes.
"""

from .Actuator import Actuator
from .Attribute import Attribute
from .Actuator import ActuatorState
from .Actuator import ActuatorPosition

"""
<object class="DoubleActingActuator">

    <!-- Port which need to be active to be in and inactive to be out -->
    <object role="in" class="ChannelAttribute">
        <uri>id16ni/wago/1</uri>
        <channel name="channel" type="tango" call="attr1" />
    </object>

    <!-- Port which need to be active to be out and inactive to be in -->
    <object role="out" class="ChannelAttribute">
        <uri>id16ni/wago/1</uri>
        <channel name="channel" type="tango" call="attr2" />
    </object>

    <!-- Default active and inactive values are 1 and 0 -->

</object>
"""


class DoubleActingActuator(Actuator):

    def __init__(self, name=None, xml_tree=None):
        Actuator.__init__(self, name, xml_tree)
        self.__state = ActuatorState.UNKNOWN
        self.__position = ActuatorPosition.UNKNOWN

    def init(self):
        expected_attributes = ["in", "out"]
        for name in expected_attributes:
            if name not in self.objects:
                raise Exception("Object '%s' is expected" % name)
            if not isinstance(self.objects[name], Attribute):
                raise Exception("Object '%s' is not a Cool Attribute" % name)

        self.__in = self.objects["in"]
        self.__out = self.objects["out"]

        self.__in.connect("valueChanged", self.__update_state)
        self.__out.connect("valueChanged", self.__update_state)
        self.__in.connect("stateChanged", self.__update_state)
        self.__out.connect("stateChanged", self.__update_state)

    def __update_state(self, useless_=None):
        if not self.__in.is_value_available() or not self.__out.is_value_available():
            self.__set_state(ActuatorState.UNKNOWN, ActuatorPosition.UNKNOWN)
            return
        value_in = int(self.__in.value())
        value_out = int(self.__out.value())
        if value_in == 1 and value_out == 0:
            self.__set_state(ActuatorState.ON, ActuatorPosition.IN)
            return
        if value_in == 0 and value_out == 1:
            self.__set_state(ActuatorState.ON, ActuatorPosition.OUT)
            return

        self.__set_state(ActuatorState.ON, ActuatorPosition.UNKNOWN)

    def __set_state(self, state=None, position=None):
        state_changed = False
        position_changed = False

        # update attributes
        if state is not None and self.__state != state:
            self.__state = state
            state_changed = True
        if position is not None and self.__position != position:
            self.__position = position
            position_changed = True

        # then send events
        if state_changed:
            self._fire_state_changed()
        if position_changed:
            self._fire_position_changed()

    def state(self):
        return self.__state

    def position(self):
        return self.__position

    def move_in(self):
        self.__in.set_value(1)
        self.__out.set_value(0)

    def move_out(self):
        self.__in.set_value(0)
        self.__out.set_value(1)

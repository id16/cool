"""Attribute object providing read/write of a value
"""
from cool.core import CObject


class AttributeValueNotAvailable(Exception):
    pass


class AttributeState:
    # The device and the attribute are not available
    UNKNOWN = "UNKNOWN"
    # The attribute is available
    ON = "ON"
    # The attribute is temporarily in read-only
    DISABLED = "DISABLED"
    # The attribute is in read-only
    READONLY = "READONLY"


class Attribute(CObject):

    def __init__(self, name=None, xml_tree=None):
        CObject.__init__(self, name, xml_tree)

    def init(self):
        CObject.init(self)

    def connect_notify(self, signal):
        if signal == "stateChanged":
            self._fire_state_changed()
        elif signal == "valueChanged":
            try:
                self._fire_value_changed()
            except AttributeValueNotAvailable:
                pass
        else:
            raise Exception("Unsupported signal %s" % signal)

    # State

    def state(self):
        raise NotImplementedError()

    def _fire_state_changed(self):
        self.emit("stateChanged", self.state())

    def is_read_only(self):
        return self.state() in [AttributeState.READONLY, AttributeState.DISABLED]

    def is_value_available(self):
        return self.state() != AttributeState.UNKNOWN

    # Value

    def value(self):
        raise NotImplementedError()

    def set_value(self, value):
        raise NotImplementedError()

    def _fire_value_changed(self):
        try:
            self.emit("valueChanged", self.value())
        except AttributeValueNotAvailable:
            pass

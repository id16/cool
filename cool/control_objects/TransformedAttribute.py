"""Attribute object provided by a a value from another attributes and a
transformation function.


<object class="TransformedAttribute">
    <object role="foo" class="AttributeMock">
        <value float="10.50" />
    </object>

    <object role="bar" class="AttributeMock">
        <value float="10.50" />
    </object>

    <!-- python evaluation using sub attributes -->
    <value eval="$foo + $bar" />
</object>

"""
import re
import logging

from .Attribute import Attribute
from .Attribute import AttributeValueNotAvailable
from .Attribute import AttributeState


_logger = logging.getLogger(__name__)

class TransformedAttribute(Attribute):

    def __init__(self, name=None, xml_tree=None):
        Attribute.__init__(self, name, xml_tree)

    def init(self):
        Attribute.init(self)
        objects_expected = set([])

        def object_replace(match):
            v = match.group(0)
            if v.startswith("$"):
                name = v[1:]
                objects_expected.add(name)
                return "self.objects['" + name + "'].value()"
            else:
                return v

        v = self.config["value/@eval"]
        if len(v) > 0:
            self.__eval = re.sub(r"\$\w+", object_replace, v[0])
            _logger.debug("Use eval: %s", self.__eval)
        else:
            self.__eval = None

        if not objects_expected.issubset(set(self.objects.keys())):
            missing = set(self.objects.keys()) - objects_expected
            raise ValueError("Object do not contains all expected attribute names: %s" % ", ".join(missing))

        self.__state = AttributeState.UNKNOWN
        self.__value = AttributeValueNotAvailable

        self.__attributes = set([])
        if self.__eval is not None:
            for object_name in objects_expected:
                attribute = self.objects[object_name]
                attribute.connect("stateChanged", self.__attribute_state_changed)
                attribute.connect("valueChanged", self.__attribute_value_changed)
                self.__attributes.add(attribute)

    # State

    def state(self):
        return self.__state

    def __merge_state(self, state1, state2):
        if state1 == state2:
            return state1
        states = set([state1, state2])
        if AttributeState.UNKNOWN in states:
            return AttributeState.UNKNOWN
        if AttributeState.DISABLED in states:
            return AttributeState.DISABLED
        if AttributeState.READONLY in states:
            return AttributeState.READONLY
        if AttributeState.ON in states:
            return AttributeState.ON
        raise Exception("Unmanaged attribute state %s and %s", state1, state2)

    def __attribute_state_changed(self, state_):
        self.__update_value()

    def __update_value(self):
        state = AttributeState.READONLY
        for attribute in self.__attributes:
            state = self.__merge_state(state, attribute.state())

        value = AttributeValueNotAvailable
        if state != AttributeState.UNKNOWN:
            if self.__eval is not None:
                try:
                    value = eval(self.__eval)
                except AttributeValueNotAvailable as e:
                    value = AttributeValueNotAvailable
                except Exception:
                    _logger.error("Error while evaluating the result", exc_info=True)
                    value = AttributeValueNotAvailable

            if value == AttributeValueNotAvailable:
                state = AttributeState.UNKNOWN

        self.__set_value(value)
        self.__set_state(state)

    def __set_state(self, state):
        if self.__state == state:
            return
        self.__state = state
        self._fire_state_changed()

    # Value

    def __attribute_value_changed(self, value_):
        self.__update_value()

    def __set_value(self, value):
        if value == self.__value:
            return
        self.__value = value

        self._fire_value_changed()

    def set_value(self, value):
        raise Exception("Write method not available for transformed attribute")

    def value(self):
        if self.__value == AttributeValueNotAvailable:
            raise AttributeValueNotAvailable("Value is not (yet) available")
        return self.__value

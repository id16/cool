"""Attribute object providing read from a command.

TODO implement the write function


<object class="CommandAttribute">
    <command name="command" call="ps" uri="chico:eh_na" type="spec"/>

    <!-- Use this unit to create a quantity -->
    <config quantityUnit="mm" />

    <!-- polling period -->
    <config period="5.0s" />
</object>

"""
import time
import logging
import cool
from cool.task_utils import task
from .Attribute import Attribute
from .Attribute import AttributeState
from .Attribute import AttributeValueNotAvailable


class CommandAttribute(Attribute):

    def __init__(self, name=None, xml_tree=None):
        Attribute.__init__(self, name, xml_tree)

    def init(self):
        Attribute.init(self)

        self.__quantity_unit = None
        v = self.config["config/@quantityUnit"]
        if len(v) != 0:
            self.__quantity_unit = v[0]

        v = self.config["config/@period"]
        v = cool.unit_registry.Quantity(v[0])
        self.__polling_period = float(v.to("s").magnitude)

        self.__value = None
        self.__state = AttributeState.UNKNOWN
        self.__requesting_result = False
        self.__have_result = False

        self.__command = self.commands.itervalues().next()
        if self.__command is None:
            raise Exception("Command is missing")
        self.__command.connect("command_finished", self.__command_finished)
        self.__command.connect("command_failed", self.__command_failed)
        self.__command.connect("__hw_connected__", self.__command_connected)

        self.__polling_thread(wait=False)

    # State

    def state(self):
        return self.__state

    def __update_state(self):
        if self.__command.ready() and self.__have_result:
            self.__set_state(AttributeState.ON)
        else:
            self.__set_state(AttributeState.UNKNOWN)

    def __set_state(self, state):
        if self.__state == state:
            return
        self.__state = state
        self._fire_state_changed()

    # Value

    @task
    def __polling_thread(self):
        while(True):
            if not self.__requesting_result and self.__command.ready():
                self.__requesting_result = True
                self.__command(wait=False)
            time.sleep(self.__polling_period)

    def __normalize_device_value(self, value):
        if self.__quantity_unit is not None:
            value = float(value)
            return cool.unit_registry.Quantity(value, self.__quantity_unit)
        else:
            return value

    def __command_connected(self, is_connected):
        if not is_connected:
            self.__have_result = False
            self.__value = None
            self.__update_state()

    def __command_failed(self, exception):
        logging.getLogger(__name__).exception(exception)
        self.__requesting_result = False

    def __command_finished(self, value):
        self.__requesting_result = False
        value = self.__normalize_device_value(value)
        if value == self.__value:
            return
        self.__value = value

        if self.state() == AttributeState.UNKNOWN:
            self.__update_state()

        if not self.__have_result:
            self.__have_result = True
            self.__update_state()

        self._fire_value_changed()

    def set_value(self, value):
        raise Exception("Write function is not available")

    def value(self):
        if not self.__have_result:
            raise AttributeValueNotAvailable("Value not available")
        return self.__value

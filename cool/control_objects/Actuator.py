"""Actuator control object
"""
from cool.core import CObject


class ActuatorState:
    # The state of the device is unknown
    UNKNOWN = "UNKNOWN"
    # The device is working
    ON = "ON"
    # The device is moving
    MOVING = "MOVING"
    # The device is disabled by something. It is not possible to do an action on it
    DISABLED = "DISABLED"
    # The device is in fault
    FAULT = "FAULT"


class ActuatorPosition:
    # The device is in
    IN = "IN"
    # The device is out
    OUT = "OUT"
    # The device position is unknown
    UNKNOWN = "UNKNOWN"


class Actuator(CObject):

    def __init__(self, *args, **kwargs):
        CObject.__init__(self, *args, **kwargs)

    def init(self):
        pass

    def connect_notify(self, signal):
        if signal == "stateChanged":
            self._fire_state_changed()
        elif signal == "positionChanged":
            self._fire_position_changed()
        else:
            raise Exception("Unsupported signal %s" % signal)

    def state(self):
        raise NotImplementedError()

    def position(self):
        raise NotImplementedError()

    def is_in(self):
        return self.position() == ActuatorPosition.IN

    def is_out(self):
        return self.position() == ActuatorPosition.OUT

    def move_in(self):
        raise NotImplementedError()

    def move_out(self):
        raise NotImplementedError()

    def _fire_state_changed(self, state=None):
        if state is None:
            state = self.state()
        self.emit("stateChanged", state)

    def _fire_position_changed(self, position=None):
        if position is None:
            position = self.position()
        self.emit("positionChanged", position)

"""ShutterMock control object

<object class="ShutterMock">
    <!-- position of the motor. It is mandatory -->
    <config name="initial_state" value="UNKNOWN" />

    <!-- Delay between 2 state used. It is mandatory -->
    <config name="delay" value="0.1" />

    <!-- Assign an initial movement. It is optional -->
    <config name="requested_state" value="OPENED" />
</object>
"""

import time
import logging
import gevent.event

from cool.core import CObject
from cool.task_utils import task
from cool.task_utils import cleanup
from .Shutter import ShutterState


class ShutterMock(CObject):
    """Create a virtual shutter.
    You can control the initial_state, the delay changes of states, and an
    already requested__state (which means the shutter is already moving)
    """
    def __init__(self, *args, **kwargs):
        CObject.__init__(self, *args, **kwargs)
        self.__thread = None
        self.__state = ShutterState.UNKNOWN
        self.__is_moving = gevent.event.Event()
        self.__is_not_moving = gevent.event.Event()

    def init(self):
        # mandatory configuration
        v = self.config["config[@name='initial_state']/@value"][0]
        if v == ShutterState.DISABLED:
            self.__is_enabled = False
            self.__physical_state = ShutterState.CLOSED
            self.__state = ShutterState.DISABLED
        else:
            self.__is_enabled = True
            self.__physical_state = v
            self.__state = v

        v = self.config["config[@name='delay']/@value"][0]
        self.__delay = float(v)
        assert(self.__delay > 0)

        # optional configuration
        requested_state = None
        v = self.config["config[@name='requested_state']/@value"]
        if len(v) == 1:
            requested_state = v[0]

        logging.getLogger(__name__).debug("shutter_mock: state=%s, delay=%s, requested_state=%s", self.__state, self.__delay, requested_state)

        self.__next_states = []
        if requested_state is not None:
            if requested_state == ShutterState.OPENED:
                self.open()
            elif requested_state == ShutterState.CLOSED:
                self.close()
            else:
                raise Exception("Requested value '%s' is not supported" % requested_state)
        else:
            self.__is_not_moving.set()

    def connect_notify(self, signal):
        if signal == "stateChanged":
            self.__fire_state_changed(self.__state)
        elif signal == "statusChanged":
            # unsupported
            pass
        else:
            raise Exception("Unsupported signal %s" % signal)

    def state(self):
        return self.__state

    def is_moving(self):
        return self.__is_moving.is_set()

    def set_enabled(self, is_enabled):
        self.__is_enabled = is_enabled
        self.__update_state()

    def is_enabled(self):
        return self.__is_enabled

    def __set_physical_state(self, state):
        self.__physical_state = state
        self.__update_state()

    def __update_state(self):
        state = self.__physical_state
        if not self.__is_enabled:
            state = ShutterState.DISABLED
        self.set_state(state)

    def set_state(self, state):
        if self.__state == state:
            return
        self.__state = state
        self.__fire_state_changed(state)

    def __fire_state_changed(self, state):
        self.emit("stateChanged", state)

    def __start_thread(self):
        self.__thread = self.__loop_thread(wait=False)
        self.__is_moving.wait()

    def _wait_while_moving(self):
        """Wait the end of the move of the shutter. Especially create for unit tests"""
        self.__is_not_moving.wait()

    def __stop_thread(self):
        if self.__thread:
            self.__thread.kill()

    def is_open(self):
        return self.__state == ShutterState.OPENED

    def is_closed(self):
        return self.__state in [ShutterState.FAULT, ShutterState.DISABLED, ShutterState.CLOSED]

    def open(self):
        if not self.__is_enabled:
            raise Exception("The shutter is disabled")
        if self.__is_moving.is_set():
            raise Exception("The shutter is still moving")
        if self.__state == ShutterState.OPENED:
            self.__next_states = []
            return
        self.__next_states = [ShutterState.MOVING, ShutterState.OPENED]
        self.__start_thread()

    def close(self):
        if not self.__is_enabled:
            raise Exception("The shutter is disabled")
        if self.__is_moving.is_set():
            raise Exception("The shutter is still moving")
        if self.__state == ShutterState.CLOSED:
            self.__next_states = []
            return
        self.__next_states = [ShutterState.MOVING, ShutterState.CLOSED]
        self.__start_thread()

    @task
    def __loop_thread(self):
        def set_final_state():
            self.__is_moving.clear()
            self.__is_not_moving.set()

        with cleanup(set_final_state):

            self.__is_not_moving.clear()
            new_state = self.__next_states.pop(0)
            self.__set_physical_state(new_state)
            self.__is_moving.set()

            while len(self.__next_states) > 0:
                new_state = self.__next_states.pop(0)
                self.__set_physical_state(new_state)
                time.sleep(self.__delay)

#!/usr/bin/python
# -*- coding: utf-8 -*-

import cool
from cool.core import CObject
from SpecClient_gevent import Spec
import weakref

LOCK_MANAGER_XML = """<object class="EsrfSpecLockManager">

    <!-- Constant -->
    <channel name="locked-value" call="LOCK_ON" uri="VERSION" type="spec"/>
    <channel name="unlocked-value" call="LOCK_OFF" uri="VERSION" type="spec"/>

    <!-- Pseudo counter configuration -->
    <channel name="lock-table" call="LOCKTABLE" uri="VERSION" type="spec"/>

</object>
"""

SPEC_INSTANCES = weakref.WeakValueDictionary()


def get_instance(version, in_thread=False):
    """Returns a singleton instance of EsrfSpecLockManager
    :rtype: EsrfSpecLockManager
    """
    if version in SPEC_INSTANCES:
        return SPEC_INSTANCES[version]
    else:
        xml = LOCK_MANAGER_XML.replace("VERSION", version)
        instance = cool.get_fromstring(xml, in_thread)
        SPEC_INSTANCES[version] = instance
        return instance


class EsrfSpecLockManager(CObject):
    """Cool object which provide changes on the ESRF Spec lock table of motors.

    This information is used by EsrfSpecMotor to update it's status.
    """

    def __init__(self, *args, **kwargs):
        CObject.__init__(self, *args, **kwargs)

    def init(self):
        """Initialize object according to configuration"""

        self.__lock_table = {}
        self.__index_mapping = {}

        self.__locked_value = self.channels["locked-value"]
        self.__lock_table_channel = self.channels["lock-table"]

        if hasattr(self.__lock_table_channel, "version"):
            self.__spec_obj = Spec.Spec(self.__lock_table_channel.version)

        self.__lock_table_channel.connect("update", self.__lock_table_updated)
        if self.__lock_table_channel.ready():
            self.__lock_table_updated(self.__lock_table_channel.value())

    def _name_from_index(self, index):
        """
        Returns the motor name from its index.

        It caches the Spec index and access to it only if the index is unknown.
        If there is a hard configuration change on the Spec server the
        mapping will anyway be wrong.
        """
        if index not in self.__index_mapping:
            self._update_index_mapping()
        return self.__index_mapping[index]

    def _update_index_mapping(self):
        """
        Updates the index/motor-name mapping
        """
        mne = self.__spec_obj.getMotorsMne()
        self.__index_mapping = {}
        for i in xrange(0, len(mne)):
            self.__index_mapping[str(i)] = mne[i]

    def __lock_table_updated(self, spec_table):
        """Fire table changes."""
        for num, locked in self.__lock_table_channel.value().iteritems():
            name = self._name_from_index(str(num))
            old_locked = self.is_locked(name)
            locked = locked == str(self.__locked_value.value())
            if locked != old_locked:
                self.__lock_table[name] = locked
                self.emit("motor_%s_changed" % name, locked)
        self.emit("motors_changed")

    def lock_table(self):
        """Returns a copy of the full lock table (motor name with locked
        status)"""
        return dict(self.__lock_table)

    def is_locked(self, name):
        """Returns the lock status from a motor name. If the motor name is
        unknown it returns False.

        :rtype: bool
        """
        if name in self.__lock_table:
            return self.__lock_table[name]
        else:
            return False

    def connect_notify(self, signal):
        """Called when a client connecting to an event"""
        if signal == "motors_changed":
            self.emit("motors_changed")
        else:
            elements = signal.split("_")
            if len(elements) == 3 and elements[0] == "motor" and elements[2] == "changed":
                name = elements[1]
                if name in self.__lock_table:
                    self.emit("motor_%s_changed" % name, self.is_locked(name))
                return

            raise Exception("%s is not an expected event" % signal)

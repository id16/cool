"""Shutter control object

<object class = "Shutter" username = "ShutterMS">
   <uri>blissdb:spec</uri>
   <command name="open" type="spec" call="msopen"/>
   <command name="close" type="spec" call="msclose"/>
   <channel name="state" type="spec" call="msstate"/>
   <channel name="status" type="spec" call="msstatuss"/>
</object>

<object class = "Shutter">
   <uri>d29/bsh/1</uri>
   <command name="open" type="tango" call="Open"/>
   <command name="close" type="tango" call="Close"/>
   <channel name="state" type="tango" call="State" polling="1000"/>
   <channel name="status" type="tango" call="Status"/>
</object>

"""
from cool.core import CObject


class ShutterState:
    # The device is opened
    OPENED = "OPENED"
    # The device is closed
    CLOSED = "CLOSED"
    # It dont know the position of the device
    UNKNOWN = "UNKNOWN"
    # The device is moving
    MOVING = "MOVING"
    # The device is disabled by something, and the shutter is closed
    DISABLED = "DISABLED"
    # The device is in fault, and the shutter is closed
    FAULT = "FAULT"


class Shutter(CObject):
    # Tango Shutterstate
    shutterState = {
        0: 'ON',
        1: 'OFF',
        2: ShutterState.CLOSED,
        3: ShutterState.OPENED,
        4: 'INSERT',
        5: 'EXTRACT',
        6: ShutterState.MOVING,
        7: 'STANDBY',
        8: 'FAULT',
        9: 'INIT',
        10: 'RUNNING',
        11: 'ALARM',
        12: 'DISABLED',
        13: ShutterState.UNKNOWN,
        - 1: 'ERROR'
    }

    def __init__(self, *args, **kwargs):
        CObject.__init__(self, *args, **kwargs)

    def init(self):
        self.shutter_state = ShutterState.UNKNOWN
        self.previous_status = ""
        self.channels["state"].connect("update", self.__state_changed)
        self.channels["status"].connect("update", self.__status_changed)

    def connect_notify(self, signal):
        if signal == "stateChanged":
            self.__fire_state_changed(self.shutter_state)
        elif signal == "statusChanged":
            self.__fire_status_changed(self.previous_status)
        else:
            raise Exception("Unsupported signal %s" % signal)

    def state(self):
        return self.shutter_state

    def is_open(self):
        return self.shutter_state == ShutterState.OPENED

    def is_closed(self):
        return self.shutter_state in [ShutterState.FAULT, ShutterState.DISABLED, ShutterState.CLOSED]

    def open(self):
        self.commands['open']()

    def close(self):
        self.commands['close']()

    def __status_changed(self, status):
        self.previous_status = status
        self.__fire_status_changed(status)

    def __fire_status_changed(self, status):
        self.emit("statusChanged", status)

    def __state_changed(self, state):
        shutterState2name = Shutter.shutterState
        if state is None:
            state = 13
        else:
            state = int(state)
        self.shutter_state = shutterState2name.get(state, ShutterState.UNKNOWN)
        self.__fire_state_changed(self.shutter_state)

    def __fire_state_changed(self, state):
        self.emit("stateChanged", state)

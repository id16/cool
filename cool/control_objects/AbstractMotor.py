import cool
from .Motor import Motor


class AbstractMotor(Motor):

    def move(self, pos, **kwargs):
        """Move the motor to a position

        :param pos: Requested new position
        :type pos: pint.unit.Quantity
        """
        if isinstance(pos, (int, long, float)):
            pos = cool.unit_registry.Quantity(pos)
        hw_pos = pos.to(self._position_unit())
        hw_pos = float(hw_pos.magnitude)
        self._hw_move(hw_pos, **kwargs)

    def _hw_move(self, pos, **kwargs):
        """Move the motor to a position

        :param pos: Requested new position
        :type pos: Float
        """
        raise NotImplementedError()

    def move_relative(self, rel_pos, **kwargs):
        """Move the motor to a relative position according to the current
        one

        :param pos: Requested relative movement
        :type rel_pos: pint.unit.Quantity
        """
        if isinstance(rel_pos, (int, long, float)):
            rel_pos = cool.unit_registry.Quantity(rel_pos)
        hw_pos = rel_pos.to(self._position_unit())
        hw_pos = float(hw_pos.magnitude)
        self._hw_move_relative(hw_pos, **kwargs)

    def _hw_move_relative(self, rel_pos, **kwargs):
        """Move the motor to a relative position according to the current
        one

        :param pos: Requested relative movement
        :type rel_pos: float
        """
        raise NotImplementedError()

    def position(self):
        """Return the current position

        :rtype: pint.unit.Quantity
        """
        hw_pos = self._hw_position()
        return cool.unit_registry.Quantity(hw_pos, self._position_unit())

    def _hw_to_position(self, pos):
        """Return a Cool device position from an hardware position

        :param pos: A position
        :type pos: float
        :rtype: pint.unit.Quantity
        """
        return cool.unit_registry.Quantity(pos, self._position_unit())

    def _hw_position(self):
        """Return the current position

        :rtype: float
        """
        raise NotImplementedError()

    def _position_unit(self):
        """Return the hardware unit used for the position

        :rtype: pint.unit.UnitsContainer
        """
        raise NotImplementedError()

    def set_limits(self, limits):
        """Apply new limits to the motor from a min,max tuple

        :type limits: (pint.unit.Quantity, pint.unit.Quantity)
        """
        min_, max_ = limits
        if isinstance(min_, (int, long, float)):
            min_ = cool.unit_registry.Quantity(min_)
        if isinstance(max, (int, long, float)):
            max_ = cool.unit_registry.Quantity(max)
        hw_min = min_.to(self._position_unit())
        hw_max = max_.to(self._position_unit())
        hw_min = float(hw_min.magnitude)
        hw_max = float(hw_max.magnitude)
        self._hw_set_limits((hw_min, hw_max))

    def _hw_set_limits(self, limits):
        """Apply new limits to the motor from a min,max tuple

        :param limits: Requested new limits
        :type pos: (float, float)
        """
        raise NotImplementedError()

    def limits(self):
        """Return the position limits (min, max)

        :rtype: (pint.unit.Quantity, pint.unit.Quantity)
        """
        l = self._hw_limits()
        l = self._hw_to_position(l[0]), self._hw_to_position(l[1])
        return l

    def _hw_limits(self):
        """Return the hardware position limits (min, max)

        :rtype: (float, float)
        """
        raise NotImplementedError()

"""Double acting actuator control object

To move this actuator it is needed to do an action in 2 attributes.
"""

from .Actuator import Actuator
from .Actuator import ActuatorState

"""
<object class="ProtectedActuator">

    <!-- A reference to a safety shutter. This shutter must be closed to be
        allowed to move the discrete motor -->
    <object href="name_of_the_xml_file" role="safety_shutter" />

    <object role="actuator" class="DoubleActingActuator">
        <!-- Port which need to be active to be in and inactive to be out -->
        <object role="in" class="ChannelAttribute">
            <uri>id16ni/wago/1</uri>
            <channel name="channel" type="tango" call="attr1" />
        </object>

        <!-- Port which need to be active to be out and inactive to be in -->
        <object role="out" class="ChannelAttribute">
            <uri>id16ni/wago/1</uri>
            <channel name="channel" type="tango" call="attr2" />
        </object>

        <!-- Default active and inactive values are 1 and 0 -->
    </object>

</object>
"""


class ProtectedActuator(Actuator):

    def __init__(self, name=None, xml_tree=None):
        Actuator.__init__(self, name, xml_tree)
        self.__state = ActuatorState.UNKNOWN

    def init(self):
        """Initialize the component. Mostly read the configuration."""
        if "safety_shutter" in self.objects:
            self.__safety_shutter = self.objects["safety_shutter"]
        else:
            self.__safety_shutter = None

        self.__actuator = self.objects["actuator"]
        self.__actuator.connect("stateChanged", self.__actuator_state_changed)
        self.__actuator.connect("positionChanged", self.__actuator_position_changed)

        if self.__safety_shutter:
            self.__safety_shutter.connect("stateChanged", self.__shutter_state_changed)

    def __shutter_state_changed(self, state):
        self.__update_state()

    def __actuator_state_changed(self, state):
        self.__update_state()

    def __actuator_position_changed(self, position):
        self._fire_position_changed(position)

    def __is_move_available(self):
        if self.__safety_shutter is not None:
            # FIXME We should request the real state and not a cached one
            if not self.__safety_shutter.is_closed():
                return False
        return True

    def __update_state(self):
        if not self.__is_move_available():
            self.__set_state(ActuatorState.DISABLED)
        else:
            self.__set_state(self.__actuator.state())

    def __set_state(self, state):
        if state == self.__state:
            return
        self.__state = state
        self._fire_state_changed()

    def state(self):
        return self.__state

    def position(self):
        return self.__actuator.position()

    def move_in(self):
        if not self.__is_move_available():
            raise Exception("Move aborted. Safety shutter is not closed")
        self.__actuator.move_in()

    def move_out(self):
        if not self.__is_move_available():
            raise Exception("Move aborted. Safety shutter is not closed")
        self.__actuator.move_out()

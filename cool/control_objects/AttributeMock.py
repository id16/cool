"""AttributeMock control object

It allows to change the value and state at real time.
"""

from .ConstAttribute import ConstAttribute
from .Attribute import AttributeState


class AttributeMock(ConstAttribute):

    def __init__(self, name=None, xml_tree=None, value=None, state=None):
        ConstAttribute.__init__(self, name, xml_tree)
        self.__value = value
        self.__state = state

    def init(self):
        v = self.config["value"]
        if self.__value is None and len(v) != 0:
            ConstAttribute.init(self)
            self.__value = ConstAttribute.value(self)
        if self.__state is None:
            self.__state = AttributeState.ON

    def set_state(self, state):
        if self.__state == state:
            return
        self.__state = state
        self._fire_state_changed()

    def state(self):
        return self.__state

    def set_value(self, value):
        if self.__value == value:
            return
        self.__value = value
        self._fire_value_changed()

    def value(self):
        return self.__value

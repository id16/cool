from command_channel import Command, Channel

from SpecClient_gevent import SpecConnectionsManager
from SpecClient_gevent.SpecCommand import SpecCommandA
from SpecClient_gevent.SpecVariable import SpecVariableA
from SpecClient_gevent import SpecVariable

_spec_connections_manager = SpecConnectionsManager.SpecConnectionsManager()


class SpecCommand(Command):
    def __init__(self, name, command, version, username=None, **kwargs):
        Command.__init__(self, name, username, **kwargs)

        self.version = version
        callbacks = {'connected': self.hardware_connected, 'disconnected': self.hardware_disconnected, 'statusChanged': self._spec_status_changed}
        self._spec_command = SpecCommandA(command, callbacks=callbacks)

    def init(self):
        self._spec_command.connectToSpec(self.version)

    def _spec_status_changed(self, ready):
        self.emit("status", ready)

    def _execute(self, *args, **kwargs):
        params = {
            'wait': True,
            'callback': kwargs.get("callback", self._spec_callback),
            'error_callback': kwargs.get("error_callback", self._spec_error_callback)
        }
        return self._spec_command(*args, **params)

    def abort(self):
        self._spec_command.abort()

    def _spec_callback(self, result):
        pass

    def _spec_error_callback(self, error):
        pass


class SpecChannel(Channel):
    def __init__(self, name, varname, version, username=None, dispatchMode=None, **kwargs):
        Channel.__init__(self, name, username, **kwargs)

        self.var_name = varname
        self.version = version
        if dispatchMode:
            self.dispatch_mode = dispatchMode
        else:
            self.dispatch_mode = SpecVariable.FIREEVENT
        callbacks = {
            'connected': self.hardware_connected,
            'disconnected': self.hardware_disconnected,
            'update': self._spec_update
        }
        self._spec_variable = SpecVariableA(callbacks=callbacks)

    def init(self):
        self._spec_variable.connectToSpec(self.var_name, self.version, self.dispatch_mode)

    def _spec_update(self, value):
        self.update(value)

    def value(self):
        return self._spec_variable.getValue()

    def set_value(self, value):
        return self._spec_variable.setValue(value)

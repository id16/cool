import unittest
import time
import cool
from cool.control_objects.Attribute import AttributeState
from . import test

xml = """<object class="CommandAttribute">
    <command type="mock" name="command" uri="" />
    <!-- polling period -->
    <config period="0.1s" />
</object>
"""


xml_quantityUnit = """<object class="CommandAttribute">
    <command type="mock" name="command" uri="" />
    <!-- Use this unit to create a quantity -->
    <config quantityUnit="mm" />
    <!-- polling period -->
    <config period="0.1s" />
</object>
"""


class TestCommandAttribute(unittest.TestCase):

    def setUp(self):
        cool = test.setup()
        self.device = cool.get_fromstring(xml)

    def test_value(self):

        def callback():
            return 10

        self.device.commands["command"].set_callback(callback)
        time.sleep(0.2)

        self.assertEquals(self.device.value(), 10)

    def test_event(self):

        self._index = 0
        self._result_list = [0, 0, 0, "aaa", "aaa", 0.5]

        def callback():
            i = self._index
            self._index = self._index + 1
            if i < len(self._result_list):
                return self._result_list[i]
            else:
                return self._result_list[-1]

        def events(value):
            self.__events.append(value)

        self.__events = []
        self.device.connect("valueChanged", events)
        self.device.commands["command"].set_callback(callback)

        time.sleep(1)

        self.assertListEqual(self.__events, [0, "aaa", 0.5])
        self.device.commands["command"].set_callback(None)
        del self._index
        del self._result_list
        del self.__events

    def test_event_reconnexion(self):

        def callback():
            return 1

        def events(value):
            self.__events.append(value)

        def state(value):
            self.__states.append(value)

        self.__events = []
        self.__states = []

        self.device.connect("valueChanged", events)
        self.device.connect("stateChanged", state)
        self.device.commands["command"].set_callback(callback)

        time.sleep(0.5)
        self.device.commands["command"].hardware_disconnected()
        time.sleep(0.5)
        self.device.commands["command"].hardware_connected()
        time.sleep(0.5)

        self.assertListEqual(self.__states, [AttributeState.UNKNOWN, AttributeState.ON, AttributeState.UNKNOWN, AttributeState.ON])
        self.device.commands["command"].set_callback(None)
        del self.__states

#    def test_failed_command(self):
#
#        def callback():
#            raise Exception("Bag!!")
#
#        self.device.commands["command"].set_callback(callback)
#
#        self.device.value()


class TestCommandAttribute_QuantityUnit(unittest.TestCase):

    def setUp(self):
        cool = test.setup()
        self.device = cool.get_fromstring(xml_quantityUnit)

    def test_value(self):

        def callback():
            return 10

        self.device.commands["command"].set_callback(callback)
        time.sleep(0.2)

        self.assertEquals(self.device.value(), cool.unit_registry.Quantity("10mm"))


if __name__ == '__main__':
    unittest.main()

import unittest
import cool
import time
from cool.mock import MockChannel
from cool.control_objects.ChannelAttribute import ChannelAttribute
from . import test

xml = """<object class="ChannelAttribute">
    <channel name="channel" type="mock" value="1" />
</object>
"""

xml_quantity_unit = """
<object class="ChannelAttribute">
    <channel name="channel" type="mock" value="1" />
    <config quantityUnit="mm" />
</object>
"""


class TestChannelAttribute(unittest.TestCase):

    def setUp(self):
        cool = test.setup()
        self.device = cool.get_fromstring(xml)

    def test_value(self):
        self.assertEquals(self.device.value(), 1)

    def test_set_value(self):
        self.device.set_value(10)
        time.sleep(0.1)
        self.assertEquals(self.device.channels["channel"].value(), 10)

    def test_event(self):

        expected = [1, 10]

        def events(value):
            self.__events.append(value)

        self.__events = []
        self.device.connect("valueChanged", events)
        self.device.channels["channel"].set_value(10)
        time.sleep(0.1)
        self.assertListEqual(self.__events, expected)
        del self.__events


class TestChannelAttribute_QuantityUnit(unittest.TestCase):

    def setUp(self):
        cool = test.setup()
        self.device = cool.get_fromstring(xml_quantity_unit)

    def test_value(self):
        self.assertEquals(self.device.value(), cool.unit_registry.Quantity("1mm"))

    def test_set_value(self):
        self.device.set_value(cool.unit_registry.Quantity("10mm"))
        self.assertEquals(self.device.channels["channel"].value(), 10)

    def test_set_value2(self):
        self.device.set_value(cool.unit_registry.Quantity("10cm"))
        self.assertEquals(self.device.channels["channel"].value(), 100)

    def test_event(self):

        expected = [cool.unit_registry.Quantity("1mm"), cool.unit_registry.Quantity("10mm")]

        def events(value):
            self.__events.append(value)

        self.__events = []
        self.device.connect("valueChanged", events)
        self.device.channels["channel"].set_value(10)
        time.sleep(0.1)
        self.assertListEqual(self.__events, expected)
        del self.__events


class TestChannelAttribute_Programmatic(unittest.TestCase):

    def test_normal(self):
        channel = MockChannel("foo", value=1)
        attribute = ChannelAttribute(channel=channel)
        self.assertEquals(attribute.value(), 1)
        attribute.set_value(10)
        time.sleep(0.1)
        self.assertEquals(channel.value(), 10)

    def test_quantity(self):
        channel = MockChannel("foo", value=1)
        attribute = ChannelAttribute(channel=channel, quantity_unit=cool.unit_registry.mm)
        self.assertEquals(attribute.value(), cool.unit_registry.Quantity("1mm"))
        attribute.set_value(cool.unit_registry.Quantity("10mm"))
        time.sleep(0.1)
        self.assertEquals(channel.value(), 10)

if __name__ == '__main__':
    unittest.main()

import sys
import os
import contextlib
import logging


def setup():
    xml_files_dir = os.path.abspath(os.path.dirname(__file__))
    if __name__.startswith("cool."):
        import cool
    else:
        cooldir = os.path.join(xml_files_dir, "..")
        sys.path.insert(0, cooldir)
        cool = __import__("cool.config", globals(), locals(), [])

    repository = xml_files_dir + "/resources"
    cool.config.set_xml_files_path(repository)
    return cool


@contextlib.contextmanager
def inhibit_log(name):
    logger = logging.getLogger(name)
    try:
        logger.setLevel(logging.CRITICAL)
        logger.propagate = False
        yield
    finally:
        logger.disabled = False
        logger.propagate = True

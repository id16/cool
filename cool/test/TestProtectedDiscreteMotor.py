import unittest
from cool.control_objects.Motor import MotorState
from . import test

xml = """<object class="ProtectedDiscreteMotor">

    <object class="MotorMock" role="motor">
        <config name="initial_value" value="5" />
        <config name="min_value" value="0" />
        <config name="max_value" value="10" />
        <config name="step" value="0.1" />
        <config name="delay" value="0.01" />
    </object>

    <position name="a" offset="0.5" />
    <position name="origin" offset="5" />

    <object class="ShutterMock" role="safety_shutter">
        <config name="initial_state" value="UNKNOWN" />
        <config name="delay" value="0.001" />
    </object>

</object>"""

xml_no_shutter = """<object class="ProtectedDiscreteMotor">

    <object class="MotorMock" role="motor">
        <config name="initial_value" value="5" />
        <config name="min_value" value="0" />
        <config name="max_value" value="10" />
        <config name="step" value="0.1" />
        <config name="delay" value="0.01" />
    </object>

    <position name="a" offset="0.5" />
    <position name="origin" offset="5" />

</object>"""


class TestProtectedDiscreteMotor(unittest.TestCase):

    def setUp(self):
        cool = test.setup()
        self.motor = cool.get_fromstring(xml)

    def test_protected_when_unknown(self):
        try:
            self.motor.move_position_id("a")
            self.fail()
        except:
            self.assertTrue(True)

    def test_protected_when_opened(self):
        self.motor._safety_shutter().open()
        self.motor._safety_shutter()._wait_while_moving()
        try:
            self.motor.move_position_id("a")
            self.fail()
        except:
            self.assertTrue(True)
        self.assertEquals(self.motor.state(), MotorState.DISABLED)

    def test_move_available(self):
        self.motor._safety_shutter().close()
        self.motor._safety_shutter()._wait_while_moving()
        self.motor.move_position_id("a")
        self.assertTrue(True)

    def test_move_available_when_disabled(self):
        self.motor._safety_shutter().set_enabled(False)
        self.motor._safety_shutter()._wait_while_moving()
        self.motor.move_position_id("a")
        self.assertTrue(True)

    def test_direct_move_protected(self):
        self.motor._safety_shutter().open()
        self.motor._safety_shutter()._wait_while_moving()
        try:
            self.motor.debug_move(3.5)
            self.fail()
        except:
            self.assertTrue(True)

    def test_direct_move_available(self):
        self.motor._safety_shutter().close()
        self.motor._safety_shutter()._wait_while_moving()
        self.motor.debug_move(3.5)
        self.assertTrue(True)


class TestProtectedDiscreteMotor_NoShutter(unittest.TestCase):

    def setUp(self):
        cool = test.setup()
        self.motor = cool.get_fromstring(xml_no_shutter)

    def test_move_available(self):
        self.motor.move_position_id("a")
        self.assertTrue(True)


if __name__ == '__main__':
    unittest.main()

import unittest
import time
from cool.control_objects.Actuator import ActuatorState
from . import test

xml = """
<object class="ProtectedActuator">

    <object role="actuator" class="DoubleActingActuator">
        <!-- Port which need to be active to be in and inactive to be out -->
        <object role="in" class="AttributeMock">
            <value integer="1" />
        </object>

        <!-- Port which need to be active to be out and inactive to be in -->
        <object role="out" class="AttributeMock">
            <value integer="0" />
        </object>
    </object>

    <object class="ShutterMock" role="safety_shutter">
        <config name="initial_state" value="UNKNOWN" />
        <config name="delay" value="0.001" />
    </object>

</object>
"""

xml_no_shutter = """
<object class="ProtectedActuator">

    <object role="actuator" class="DoubleActingActuator">
        <!-- Port which need to be active to be in and inactive to be out -->
        <object role="in" class="AttributeMock">
            <value integer="1" />
        </object>

        <!-- Port which need to be active to be out and inactive to be in -->
        <object role="out" class="AttributeMock">
            <value integer="0" />
        </object>
    </object>

</object>
"""


class TestProtectedActuator(unittest.TestCase):

    def setUp(self):
        cool = test.setup()
        self.device = cool.get_fromstring(xml)

    def test_protected_when_unknown(self):
        try:
            self.device.move_in("a")
            self.fail()
        except:
            self.assertTrue(True)

    def test_protected_when_opened(self):
        self.device.objects["safety_shutter"].open()
        self.device.objects["safety_shutter"]._wait_while_moving()
        try:
            self.device.move_in()
            self.fail()
        except:
            self.assertTrue(True)
        self.assertEquals(self.device.state(), ActuatorState.DISABLED)

    def test_move_available(self):
        self.device.objects["safety_shutter"].close()
        self.device.objects["safety_shutter"]._wait_while_moving()
        self.device.move_in()
        self.device.move_out()
        self.assertTrue(True)

    def test_move_available_when_disabled(self):
        self.device.objects["safety_shutter"].set_enabled(False)
        self.device.objects["safety_shutter"]._wait_while_moving()
        self.device.move_in()
        self.device.move_out()
        self.assertTrue(True)


class TestProtectedActuator_NoShutter(unittest.TestCase):

    def setUp(self):
        cool = test.setup()
        self.device = cool.get_fromstring(xml_no_shutter)

    def test_move_available(self):
        self.device.move_in()
        time.sleep(0.01)
        self.device.move_out()
        time.sleep(0.01)
        self.assertTrue(True)


if __name__ == '__main__':
    unittest.main()

import unittest
from cool.control_objects.Attribute import AttributeState
from . import test


xml = """<object class="TransformedAttribute">

    <object role="aaa" class="AttributeMock">
        <value integer="10" />
    </object>

    <object role="bbb" class="ConstAttribute">
        <value integer="11" />
    </object>

    <value eval="$aaa + $bbb" />

</object>
"""


class TestTransformedAttribute(unittest.TestCase):

    def setUp(self):
        cool = test.setup()
        self.device = cool.get_fromstring(xml)

    def test_declaration(self):
        self.assertEquals(self.device.value(), 21)
        self.assertEquals(self.device.state(), AttributeState.READONLY)

    def test_unknown(self):
        self.device.objects["aaa"].set_state(AttributeState.UNKNOWN)
        self.assertEquals(self.device.state(), AttributeState.UNKNOWN)
        try:
            self.device.value()
            self.fail()
        except Exception:
            pass

    def test_value_change(self):
        self.device.objects["aaa"].set_value(11)
        self.assertEquals(self.device.state(), AttributeState.READONLY)
        self.assertEquals(self.device.value(), 22)

    def test_eval_failed(self):
        with test.inhibit_log("cool.control_objects.TransformedAttribute"):
            self.device.objects["aaa"].set_value("aaa")
        self.assertEquals(self.device.state(), AttributeState.UNKNOWN)
        try:
            self.device.value()
            self.fail()
        except Exception:
            pass


if __name__ == '__main__':
    unittest.main()

import unittest
import time
from cool.control_objects.AbstractSlit import SlitState
from . import test


XML = """<object class="Slit">
    <!-- It must be a SpecMotor -->
    <object role="blade1" class="MotorMock">
        <config name="initial_value" value="5" />
        <config name="min_value" value="0" />
        <config name="max_value" value="10" />
        <config name="step" value="0.1" />
        <config name="delay" value="0.05" />
    </object>

    <object role="blade2" class="MotorMock">
        <config name="initial_value" value="6" />
        <config name="min_value" value="0" />
        <config name="max_value" value="10" />
        <config name="step" value="0.1" />
        <config name="delay" value="0.05" />
    </object>

    <object role="gap" class="MotorMock">
        <config name="initial_value" value="7" />
        <config name="min_value" value="0" />
        <config name="max_value" value="10" />
        <config name="step" value="0.1" />
        <config name="delay" value="0.05" />
    </object>

    <object role="offset" class="MotorMock">
        <config name="initial_value" value="8" />
        <config name="min_value" value="0" />
        <config name="max_value" value="10" />
        <config name="step" value="0.1" />
        <config name="delay" value="0.05" />
    </object>
</object>
"""


class TestSlit(unittest.TestCase):

    def setUp(self):
        cool = test.setup()
        self.slit = cool.get_fromstring(XML)

    def test_initial_position(self):
        self.assertEquals(self.slit.position_blade1(), 5)
        self.assertEquals(self.slit.position_blade2(), 6)
        self.assertEquals(self.slit.gap(), 7)
        self.assertEquals(self.slit.offset(), 8)
        self.assertEquals(self.slit.state(), SlitState.ON)

    def test_set_blades(self):
        requested = 4
        self.slit.move_blade1(requested)
        self.slit.move_blade2(requested)
        while self.slit.objects["blade1"].is_moving():
            time.sleep(0.01)
        while self.slit.objects["blade2"].is_moving():
            time.sleep(0.01)
        self.assertEquals(self.slit.position_blade1(), requested)
        self.assertEquals(self.slit.position_blade2(), requested)

    def test_set_slit(self):
        requested = 4
        self.slit.set_offset(requested)
        self.slit.set_gap(requested)
        while self.slit.objects["gap"].is_moving():
            time.sleep(0.01)
        while self.slit.objects["offset"].is_moving():
            time.sleep(0.01)
        self.assertEquals(self.slit.gap(), requested)
        self.assertEquals(self.slit.offset(), requested)

    def test_state_event(self):
        requested = 4
        self.lastStates = []

        def callback(state):
            self.lastStates.append(state)

        self.slit.connect("stateChanged", callback)
        # test connect_notify
        self.assertTrue(self.lastStates is not [SlitState.ON])
        self.slit.move_blade1(requested)
        while self.slit.objects["blade1"].is_moving():
            time.sleep(0.01)
        self.assertEquals(self.lastStates, [SlitState.ON, SlitState.MOVING, SlitState.ON])
        del self.lastStates

    def test_position_event(self):
        requested = 4
        self.lastPosition = None

        def callback(position):
            self.lastPosition = position

        self.slit.connect("blade1Changed", callback)
        # test connect_notify
        self.assertTrue(self.lastPosition is not None)
        self.slit.move_blade1(requested)
        while self.slit.state() == SlitState.MOVING:
            time.sleep(0.01)
        self.assertEquals(self.lastPosition, requested)
        del self.lastPosition

    def test_gap_event(self):
        requested = 4
        self.lastPosition = None

        def callback(position):
            self.lastPosition = position

        self.slit.connect("gapChanged", callback)
        # test connect_notify
        self.assertTrue(self.lastPosition is not None)
        self.slit.set_gap(requested)
        while self.slit.objects["gap"].is_moving():
            time.sleep(0.01)
        self.assertEquals(self.lastPosition, requested)
        del self.lastPosition

XML_MISSING_ROLE = """<object class="Slit">
    <!-- It must be a SpecMotor -->
    <object role="blade1" class="MotorMock">
        <config name="initial_value" value="5" />
        <config name="min_value" value="0" />
        <config name="max_value" value="10" />
        <config name="step" value="0.1" />
        <config name="delay" value="0.05" />
    </object>

    <object role="gap" class="MotorMock">
        <config name="initial_value" value="7" />
        <config name="min_value" value="0" />
        <config name="max_value" value="10" />
        <config name="step" value="0.1" />
        <config name="delay" value="0.05" />
    </object>

    <object role="offset" class="MotorMock">
        <config name="initial_value" value="8" />
        <config name="min_value" value="0" />
        <config name="max_value" value="10" />
        <config name="step" value="0.1" />
        <config name="delay" value="0.05" />
    </object>
</object>
"""

XML_WRONG_CLASS = """<object class="Slit">
    <!-- It must be a SpecMotor -->
    <object role="blade1" class="MotorMock">
        <config name="initial_value" value="5" />
        <config name="min_value" value="0" />
        <config name="max_value" value="10" />
        <config name="step" value="0.1" />
        <config name="delay" value="0.05" />
    </object>

    <object role="blade2" class="MotorMock">
        <config name="initial_value" value="6" />
        <config name="min_value" value="0" />
        <config name="max_value" value="10" />
        <config name="step" value="0.1" />
        <config name="delay" value="0.05" />
    </object>

    <object role="gap" class="ShutterMock">
        <config name="initial_state" value="UNKNOWN" />
        <config name="delay" value="0.1" />
    </object>

    <object role="offset" class="MotorMock">
        <config name="initial_value" value="8" />
        <config name="min_value" value="0" />
        <config name="max_value" value="10" />
        <config name="step" value="0.1" />
        <config name="delay" value="0.05" />
    </object>
</object>
"""


class TestSlitXml(unittest.TestCase):

    def test_missing_role(self):
        cool = test.setup()
        try:
            cool.get_fromstring(XML_MISSING_ROLE)
        except Exception:
            pass

    def test_wrong_class(self):
        cool = test.setup()
        try:
            cool.get_fromstring(XML_WRONG_CLASS)
        except Exception as e:
            self.assertTrue("motor" in e.args[0].lower())
            pass

if __name__ == '__main__':
    unittest.main()

import time
import unittest
from cool.control_objects.Attribute import AttributeState
from cool.control_objects.Actuator import ActuatorState, ActuatorPosition
from . import test

xml_integer = """
<object class="DoubleActingActuator">

    <!-- Port which need to be active to be in and inactive to be out -->
    <object role="in" class="AttributeMock">
        <value integer="1" />
    </object>

    <!-- Port which need to be active to be out and inactive to be in -->
    <object role="out" class="AttributeMock">
        <value integer="0" />
    </object>

</object>
"""

xml_float = """
<object class="DoubleActingActuator">

    <!-- Port which need to be active to be in and inactive to be out -->
    <object role="in" class="AttributeMock">
        <value float="1" />
    </object>

    <!-- Port which need to be active to be out and inactive to be in -->
    <object role="out" class="AttributeMock">
        <value float="0" />
    </object>

</object>
"""


class TestDoubleActingActuator(unittest.TestCase):

    def setUp(self):
        self.cool = test.setup()
        self.device = self.cool.get_fromstring(xml_integer)

    def test_initial_position(self):
        self.assertEquals(self.device.state(), ActuatorState.ON)

    def test_missing_device(self):
        self.device.objects["in"].set_state(AttributeState.UNKNOWN)
        time.sleep(0.01)
        self.assertEquals(self.device.state(), ActuatorState.UNKNOWN)
        self.device.objects["in"].set_state(AttributeState.ON)
        time.sleep(0.01)
        self.assertEquals(self.device.state(), ActuatorState.ON)

    def test_wrong_position(self):
        self.device.objects["in"].set_value(1)
        self.device.objects["out"].set_value(1)
        time.sleep(0.01)
        self.assertEquals(self.device.state(), ActuatorState.ON)
        self.assertEquals(self.device.position(), ActuatorPosition.UNKNOWN)

    def test_wrong_position2(self):
        self.device.objects["in"].set_value(0)
        self.device.objects["out"].set_value(0)
        time.sleep(0.01)
        self.assertEquals(self.device.state(), ActuatorState.ON)
        self.assertEquals(self.device.position(), ActuatorPosition.UNKNOWN)

    def test_in(self):
        self.device.move_in()
        time.sleep(0.01)
        self.assertEquals(self.device.state(), ActuatorState.ON)
        self.assertEquals(self.device.position(), ActuatorPosition.IN)

    def test_out(self):
        self.device.move_out()
        time.sleep(0.01)
        self.assertEquals(self.device.state(), ActuatorState.ON)
        self.assertEquals(self.device.position(), ActuatorPosition.OUT)

    def test_float_in(self):
        self.device = self.cool.get_fromstring(xml_float)
        self.device.move_in()
        time.sleep(0.01)
        self.assertEquals(self.device.state(), ActuatorState.ON)
        self.assertEquals(self.device.position(), ActuatorPosition.IN)

    def test_float_out(self):
        self.device = self.cool.get_fromstring(xml_float)
        self.device.move_out()
        time.sleep(0.01)
        self.assertEquals(self.device.state(), ActuatorState.ON)
        self.assertEquals(self.device.position(), ActuatorPosition.OUT)

    def test_state_event(self):
        self.execution_count = 0

        def callback(state):
            self.execution_count += 1
            if self.execution_count == 1:
                # The connect create an event itself
                return
            # Check event content
            self.assertEquals(state, ActuatorState.ON)
            # Check device content
            self.assertEquals(self.device.state(), ActuatorState.ON)
            self.assertEquals(self.device.position(), ActuatorPosition.IN)

        self.device.objects["in"].set_state(AttributeState.UNKNOWN)
        time.sleep(0.01)
        self.device.connect("stateChanged", callback)
        self.device.objects["in"].set_state(AttributeState.ON)
        time.sleep(0.01)

        self.assertEquals(self.execution_count, 2)
        del self.execution_count

    def test_position_event(self):
        self.execution_count = 0

        def callback(position):
            self.execution_count += 1
            if self.execution_count == 1:
                # The connect create an event itself
                return
            # check event content
            self.assertEquals(position, ActuatorPosition.IN)
            # Check device content
            self.assertEquals(self.device.state(), ActuatorState.ON)
            self.assertEquals(self.device.position(), ActuatorPosition.IN)

        self.device.objects["in"].set_value(0)
        time.sleep(0.01)
        self.device.connect("positionChanged", callback)
        self.device.objects["in"].set_value(1)
        time.sleep(0.01)

        self.assertEquals(self.execution_count, 2)
        del self.execution_count


if __name__ == '__main__':
    unittest.main()

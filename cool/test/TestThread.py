import unittest
import threading
import traceback
import sys
from . import test


class TestTangoMessup(unittest.TestCase):
    def setUp(self):
        self.cool = test.setup()

    def test_wrapping(self):
        obj = self.cool.get_fromstring("<object class='GuineaPig'/>", in_thread=True)
        self.assertTrue(obj)
        self.assertNotEqual(id(threading.current_thread()), obj.thread_id())

    def test_raise_exception(self):
        obj = self.cool.get_fromstring("<object class='GuineaPig'/>", in_thread=True)
        try:
            obj.answer("How are you?")
            self.fail()
        except:
            _exception_class, exception, tb = sys.exc_info()
            full_tb = traceback.extract_tb(tb)
            exception_tb = "".join(traceback.format_list(full_tb))
            # exception is the right one
            self.assertEquals(exception.__class__.__name__, "GuineaPigException")
            # raise line
            self.assertTrue("raise GuineaPigException(" in exception_tb)
            # raised by
            self.assertTrue("GuineaPig.py" in exception_tb)
            # caller
            self.assertTrue("TestThread.py" in exception_tb)

    def test_init(self):
        obj = self.cool.get_fromstring("<object class='GuineaPig'><config name='color' value='blue' /></object>", in_thread=True)
        self.assertEquals(obj.color(), "blue")

    def test_del(self):
        obj = self.cool.get_fromstring("<object class='GuineaPig'><config name='color' value='blue' /></object>", in_thread=True)
        obj_id = obj.obj_id
        del obj
        self.assertIsNone(self.cool.thread.objs.get(obj_id))

    def test_event(self):
        obj = self.cool.get_fromstring("<object class='GuineaPig'/>", in_thread=True)
        self.last = None

        def callback(value):
            self.last = value

        obj.connect("colorChanged", callback)
        self.assertIsNotNone(self.last)
        obj.set_color("red")
        self.assertEquals(self.last, "red")

        del self.last

if __name__ == '__main__':
    unittest.main()

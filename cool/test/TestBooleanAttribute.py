import unittest
import time
from cool.control_objects.AttributeMock import AttributeMock
from cool.control_objects.BooleanAttribute import BooleanAttribute
from . import test

xml = """
<object class="BooleanAttribute">
    <object role="attribute" class="AttributeMock">
        <value string="1" />
    </object>
</object>
"""

xml_inverted = """
<object class="BooleanAttribute">
    <object role="attribute" class="AttributeMock">
        <value string="0" />
    </object>
    <config inverted="true" />
</object>
"""


class TestBooleanAttribute(unittest.TestCase):

    def setUp(self):
        cool = test.setup()
        self.device = cool.get_fromstring(xml)

    def test_value(self):
        self.assertEquals(self.device.value(), True)

    def test_set_value(self):
        self.device.set_value(False)
        time.sleep(0.1)
        self.assertEquals(self.device.objects["attribute"].value(), "0")

    def test_event(self):

        expected = [True, False]

        def events(value):
            self.__events.append(value)

        self.__events = []
        self.device.connect("valueChanged", events)
        self.device.objects["attribute"].set_value(0)
        time.sleep(0.1)
        self.assertListEqual(self.__events, expected)
        del self.__events


class TestBooleanAttributeInverted(TestBooleanAttribute):

    def setUp(self):
        cool = test.setup()
        self.device = cool.get_fromstring(xml_inverted)

    def test_value(self):
        self.assertEquals(self.device.value(), True)

    def test_set_value(self):
        self.device.set_value(False)
        time.sleep(0.1)
        self.assertEquals(self.device.objects["attribute"].value(), "1")

    def test_event(self):

        expected = [True, False]

        def events(value):
            self.__events.append(value)

        self.__events = []
        self.device.connect("valueChanged", events)
        self.device.objects["attribute"].set_value(1)
        time.sleep(0.1)
        self.assertListEqual(self.__events, expected)
        del self.__events


class TestBooleanAttributeProgrammatic(unittest.TestCase):

    def test_normal(self):
        sub_attribute = AttributeMock(value=1)
        attribute = BooleanAttribute(attribute=sub_attribute)
        self.assertEquals(attribute.value(), True)
        attribute.set_value(False)
        time.sleep(0.1)
        self.assertEquals(sub_attribute.value(), 0)

    def test_inverted(self):
        sub_attribute = AttributeMock(value=1)
        attribute = BooleanAttribute(attribute=sub_attribute, inverted=True)
        self.assertEquals(attribute.value(), False)
        attribute.set_value(True)
        time.sleep(0.1)
        self.assertEquals(sub_attribute.value(), 0)

    def test_int_string(self):
        sub_attribute = AttributeMock(value="1")
        attribute = BooleanAttribute(attribute=sub_attribute)
        self.assertEquals(attribute.value(), True)
        attribute.set_value(False)
        time.sleep(0.1)
        self.assertEquals(sub_attribute.value(), "0")

    def test_string(self):
        sub_attribute = AttributeMock(value="true")
        attribute = BooleanAttribute(attribute=sub_attribute)
        self.assertEquals(attribute.value(), True)
        attribute.set_value(False)
        time.sleep(0.1)
        self.assertEquals(sub_attribute.value(), "false")

if __name__ == '__main__':
    unittest.main()

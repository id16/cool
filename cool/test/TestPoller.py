import unittest
import time
import os.path
from tempfile import NamedTemporaryFile
from . import test
from cool import Poller


class TestPoller(unittest.TestCase):

    def setUp(self):
        self.cool = test.setup()

    def _poll(self):
        if self._index >= len(self._input):
            return self._input[-1]
        res = self._input[self._index]
        self._index += 1
        return res

    def _value_changed(self, v):
        self._received.append(v)

    def _error(self, *args, **kwargs):
        self._err = True

    def test_poll(self):
        self._index = 0
        self._received = []
        self._err = False
        self._input = [0, 10, 10, 20, 30, 50]

        poller = Poller.poll(self._poll,
                             polling_period=100,
                             value_changed_callback=self._value_changed,
                             error_callback=self._error,
                             start_delay=100)

        time.sleep(0.1 * len(self._input) + 1)
        poller.stop()

        assert self._err == False
        assert self._received == [0, 10, 20, 30, 50]

if __name__ == '__main__':
    unittest.main()

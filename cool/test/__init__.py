"""
    Test suite which contains everything

    python -m unittest test.full
"""

# add path to be able to import from the test directory
import sys
import os
import unittest
import importlib
import logging

logging.basicConfig()


sys.path.insert(0, os.path.abspath(os.path.dirname(__file__)))
"""Register this path to be able to load the module GuineaPig from xml files.
"""

_logger = logging.getLogger(__name__)


def full():
    """
    Create a test suite with full set of modules of the directory prefixed by "Test"
    """
    suite = unittest.TestSuite()
    from os import listdir
    if __name__ == "__main__":
        root = "test"
    else:
        root = "cool.test"

    for f in listdir(os.path.dirname(__file__)):
        if f.startswith("Test") and f.endswith(".py"):
            f = f[0:-3]
            print("Add module %s to the test suite (python -m unittest cool.test.%s)" % (f, f))
            try:
                m = importlib.import_module("." + f, root)
            except ImportError:
                _logger.error("Error while importing %s", f, exc_info=True)
                continue
            suite.addTest(unittest.defaultTestLoader.loadTestsFromName(m.__name__))
    return suite

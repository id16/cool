import time
import unittest
import cool
from cool.control_objects.Motor import MotorState
from . import test

xml_dimensionless = """<object class="MotorMock">
    <!-- position of the motor. It is mandatory -->
    <config name="initial_value" value="5" />

    <!-- Minimal position of the motor. It is mandatory -->
    <config name="min_value" value="0" />

    <!-- Maximal position of the motor. It is mandatory -->
    <config name="max_value" value="10" />

    <!-- Step used to move the motor. It is mandatory -->
    <config name="step" value="0.1" />

    <!-- Delay between 2 step, in second. It is mandatory -->
    <config name="delay" value="0.01" />

    <!-- Assign an initial movement. It is optional -->
    <!--config name="requested_value" value="8" /-->
</object>
"""

xml_length = """<object class="MotorMock">
    <!-- position of the motor. It is mandatory -->
    <config name="initial_value" value="0mm" />

    <!-- Minimal position of the motor. It is mandatory -->
    <config name="min_value" value="-10mm" />

    <!-- Maximal position of the motor. It is mandatory -->
    <config name="max_value" value="10mm" />

    <!-- Step used to move the motor. It is mandatory -->
    <config name="step" value="0.1mm" />

    <!-- Delay between 2 step, in second. It is mandatory -->
    <config name="delay" value="0.01" />

</object>
"""


class TestMotorMock(unittest.TestCase):

    def setUp(self):
        cool = test.setup()
        self.motor = cool.get_fromstring(xml_dimensionless)

    def __wait_motor_finish_moving(self):
        for _i in range(0, 10):
            time.sleep(0.2)
            state = self.motor.state()
            if state != MotorState.MOVING:
                return
        raise Exception("Motor is still moving")

    def test_move_positive(self):
        requestedPosition = 6.0
        self.motor.move(requestedPosition)
        self.__wait_motor_finish_moving()
        self.assertEquals(self.motor.position(), requestedPosition)

    def test_move_negative(self):
        requestedPosition = 4.5
        self.motor.move(requestedPosition)
        self.__wait_motor_finish_moving()
        self.assertEquals(self.motor.position(), requestedPosition)

    def test_events(self):
        requestedPosition = 5.5
        self.lastPosition = None

        def callback(position):
            self.lastPosition = position

        self.motor.connect("positionChanged", callback)
        self.motor.move(requestedPosition)
        self.__wait_motor_finish_moving()
        self.assertEquals(self.lastPosition, requestedPosition)
        del self.lastPosition

    def test_state(self):
        position = 4.5
        self.motor.move(position)
        self.assertEquals(self.motor.state(), MotorState.MOVING)
        self.__wait_motor_finish_moving()
        self.assertEquals(self.motor.state(), MotorState.ON)

    def test_state_events(self):
        requestedPosition = 5.5
        self.lastState = None

        def callback(state):
            self.lastState = state

        self.motor.connect("stateChanged", callback)
        # test connect_notify
        self.assertTrue(self.lastState is not None)
        self.motor.move(requestedPosition)
        self.__wait_motor_finish_moving()
        self.assertEquals(self.lastState, MotorState.ON)
        del self.lastState

    def test_abort(self):
        requestedPosition = 0.1
        self.motor.move(requestedPosition)
        self.motor.abort()
        self.assertNotEquals(self.motor.position(), requestedPosition)
        self.assertEquals(self.motor.state(), MotorState.ON)

    def test_double_move(self):
        requestedPosition1 = 0.1
        requestedPosition2 = 6.0
        self.motor.move(requestedPosition1)
        self.motor.move(requestedPosition2)
        self.__wait_motor_finish_moving()
        self.assertEquals(self.motor.position(), requestedPosition2)

    def test_limit_min(self):
        requestedPosition = -1
        self.motor.move(requestedPosition)
        self.__wait_motor_finish_moving()
        self.assertEquals(self.motor.position(), 0)
        self.assertEquals(self.motor.state(), MotorState.ALARM)

    def test_limit_max(self):
        requestedPosition = 11
        self.motor.move(requestedPosition)
        self.__wait_motor_finish_moving()
        self.assertEquals(self.motor.position(), 10)
        self.assertEquals(self.motor.state(), MotorState.ALARM)

    def test_move_is_asynchronous(self):
        requestedPosition = 6.0

        self.assertFalse(self.motor.is_moving())
        self.motor.move(requestedPosition)
        self.assertTrue(self.motor.is_moving())
        self.__wait_motor_finish_moving()
        # the wait_motor_finish_moving is based on position...
        time.sleep(0.1)
        self.assertFalse(self.motor.is_moving())

    def test_move_on_event(self):
        self._requestedPosition = 5.5
        self._firstTime = True

        def callback(state):
            if not self._firstTime and state == MotorState.ON:
                self.motor.disconnect("stateChanged", callback)
                self.motor.move(self._requestedPosition)
            if self._firstTime:
                self._firstTime = False

        self.motor.connect("stateChanged", callback)
        # test connect_notify
        self.motor.move(5)
        self.__wait_motor_finish_moving()
        self.__wait_motor_finish_moving()
        self.assertEquals(self.motor.position(), self._requestedPosition)
        del self._requestedPosition
        del self._firstTime

    def test_move_not_available_in_off(self):
        self.motor.off()
        try:
            self.motor.move(1)
            self.fail()
        except:
            pass


class TestMotorMockLength(unittest.TestCase):

    def setUp(self):
        cool = test.setup()
        self.motor = cool.get_fromstring(xml_length)

    def __wait_motor_finish_moving(self):
        for _i in range(0, 10):
            time.sleep(0.2)
            state = self.motor.state()
            if state != MotorState.MOVING:
                return
        raise Exception("Motor is still moving")

    def test_default(self):
        pos = cool.unit_registry.Quantity("0mm")
        min_ = cool.unit_registry.Quantity("-10mm")
        max_ = cool.unit_registry.Quantity("10mm")
        self.assertEquals(self.motor.position(), pos)
        self.assertEquals(self.motor.limits(), (min_, max_))

    def test_move_mm(self):
        requestedPosition = cool.unit_registry.Quantity("1.0mm")
        self.motor.move(requestedPosition)
        self.__wait_motor_finish_moving()
        self.assertEquals(self.motor.position(), requestedPosition)

    def test_move_um(self):
        requestedPosition = cool.unit_registry.Quantity("1.0um")
        self.motor.move(requestedPosition)
        self.__wait_motor_finish_moving()
        self.assertEquals(self.motor.position(), requestedPosition)

    def test_move_wrong_dimensionless(self):
        requestedPosition = 5
        try:
            self.motor.move(requestedPosition)
            self.fail()
        except:
            pass

    def test_move_wrong_unit(self):
        requestedPosition = cool.unit_registry.Quantity("1.0rad")
        try:
            self.motor.move(requestedPosition)
            self.fail()
        except:
            pass

    def test_move_on_event(self):
        self._requestedPosition = cool.unit_registry.Quantity("6.0mm")
        self._firstTime = True

        def callback(state):
            if not self._firstTime and state == MotorState.ON:
                self.motor.disconnect("stateChanged", callback)
                self.motor.move(self._requestedPosition)
            if self._firstTime:
                self._firstTime = False

        self.motor.connect("stateChanged", callback)
        # test connect_notify
        self.motor.move(cool.unit_registry.Quantity("5.0mm"))
        self.__wait_motor_finish_moving()
        self.__wait_motor_finish_moving()
        self.assertEquals(self.motor.position(), self._requestedPosition)
        del self._requestedPosition
        del self._firstTime

if __name__ == '__main__':
    unittest.main()

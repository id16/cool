import unittest
from . import test

xml = """<object class="ShutterMock">
    <!-- position of the motor. It is mandatory -->
    <config name="initial_state" value="UNKNOWN" />

    <!-- Delay between 2 state used. It is mandatory -->
    <config name="delay" value="0.1" />
</object>"""

xml_moving = """<object class="ShutterMock">
    <!-- position of the motor. It is mandatory -->
    <config name="initial_state" value="UNKNOWN" />

    <!-- Delay between 2 state used. It is mandatory -->
    <config name="delay" value="0.1" />

    <!-- Assign an initial movement. It is optional -->
    <config name="requested_state" value="OPENED" />
</object>"""


class TestShutterMock(unittest.TestCase):

    def setUp(self):
        cool = test.setup()
        self.shutter = cool.get_fromstring(xml)

    def test_initial_position(self):
        self.assertEquals(self.shutter.state(), "UNKNOWN")

    def test_open(self):
        self.shutter.open()
        self.shutter._wait_while_moving()
        self.assertEquals(self.shutter.state(), "OPENED")

    def test_close(self):
        self.shutter.close()
        self.shutter._wait_while_moving()
        self.assertEquals(self.shutter.state(), "CLOSED")

    def test_events(self):
        self.lastState = None

        def callback(state):
            self.lastState = state

        self.shutter.connect("stateChanged", callback)
        self.shutter.close()
        self.shutter._wait_while_moving()
        self.assertEquals(self.lastState, "CLOSED")
        del self.lastState

    def test_disabled(self):
        self.shutter.set_enabled(False)
        self.assertEquals(self.shutter.state(), "DISABLED")
        try:
            self.shutter.open()
            self.fail()
        except:
            self.assertTrue(True)

    def test_initial_events(self):
        self.lastState = None

        def callback(state):
            self.lastState = state

        self.shutter.connect("stateChanged", callback)
        self.assertEquals(self.lastState, "UNKNOWN")
        del self.lastState


class TestShutterMock_Moving(unittest.TestCase):

    def setUp(self):
        cool = test.setup()
        self.shutter = cool.get_fromstring(xml_moving)

    def test_initial_position(self):
        self.shutter._wait_while_moving()
        self.assertEquals(self.shutter.state(), "OPENED")


if __name__ == '__main__':
    unittest.main()

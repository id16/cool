import unittest
import os.path
from tempfile import NamedTemporaryFile
from . import test

XML = """<object class='GuineaPig'>
    <!-- Hmmm so beautiful -->
    <config name='color' value='red' />
</object>
"""

XML_EMBEDDED = """<object class='GuineaPig'>
    <!-- Hmmm so beautiful -->
    <config name='color' value='red' />
    <object class='GuineaPig' role='fetus'>
        <config name='color' value='red' />
    </object>
</object>
"""

XML_HREF = """<object class='GuineaPig'>
    <!-- Hmmm so beautiful -->
    <config name='color' value='red' />
    <object href='%s' role="fetus" />
</object>
"""


class TestSave(unittest.TestCase):

    def setUp(self):
        self.cool = test.setup()

    def test_equi_store(self):
        obj = self.cool.get_fromstring(XML)
        obj.store()
        result = obj.config.tostring()
        # still have comment
        self.assertEquals(result.count("so beautiful"), 1)
        # still have the color property
        self.assertEquals(result.count("color"), 1)
        # still have the right color
        self.assertEquals(result.count("red"), 1)

    def test_string_cant_be_saved(self):
        obj = self.cool.get_fromstring(XML)
        try:
            obj.save()
            self.fail()
        except:
            self.assertTrue(True)

    def test_store(self):
        obj = self.cool.get_fromstring(XML)
        obj.set_color("yellow")
        obj.store()
        xml = obj.config.tostring()
        # still have comment
        self.assertEquals(xml.count("so beautiful"), 1)
        # still have the color property
        self.assertEquals(xml.count("color"), 1)
        # Have the new color
        self.assertEquals(xml.count("yellow"), 1)

    def test_store_embedded(self):
        obj = self.cool.get_fromstring(XML_EMBEDDED)
        obj.set_color("yellow")
        obj.objects["fetus"].set_color("purple")
        obj.store()
        xml = obj.config.tostring()
        self.assertEquals(xml.count("yellow"), 1)
        # Child are also stored
        self.assertEquals(xml.count("purple"), 1)

    def test_save_simple(self):
        tmpfile = NamedTemporaryFile(suffix=".xml", delete=False)
        tmpfile.write(XML)
        tmpfile.close()
        name = os.path.splitext(tmpfile.name)[0]
        name = os.path.basename(name)

        root = os.path.dirname(tmpfile.name)
        self.cool.config.set_xml_files_path(root)

        # edit
        obj = self.cool.get(name)
        obj.set_color("yellow")
        obj.save()

        # test
        obj2 = self.cool.get(name)
        self.assertEquals(obj2.color(), "yellow")

        os.unlink(tmpfile.name)

    def test_save_refs(self):
        fetusfile = NamedTemporaryFile(suffix=".xml", delete=False)
        fetusfile.write(XML)
        fetusfile.close()
        fetusname = os.path.splitext(fetusfile.name)[0]
        fetusname = os.path.basename(fetusname)

        motherfile = NamedTemporaryFile(suffix=".xml", delete=False)
        motherfile.write(XML_HREF % fetusname)
        motherfile.close()
        mothername = os.path.splitext(motherfile.name)[0]
        mothername = os.path.basename(mothername)

        root = os.path.dirname(fetusfile.name)
        self.cool.config.set_xml_files_path(root)

        # edit
        obj = self.cool.get(mothername)
        obj.set_color("yellow")
        obj.objects["fetus"].set_color("purple")
        obj.save()

        # test
        obj2 = self.cool.get(mothername)
        self.assertEquals(obj2.color(), "yellow")
        obj3 = self.cool.get(fetusname)
        self.assertEquals(obj3.color(), "purple")

        os.unlink(motherfile.name)
        os.unlink(fetusfile.name)

if __name__ == '__main__':
    unittest.main()

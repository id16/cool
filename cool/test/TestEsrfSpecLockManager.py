import unittest
from cool.control_objects.EsrfSpecLockManager import EsrfSpecLockManager
from . import test


xml = """
<object class="EsrfSpecLockManager">

    <channel type="mock" name="locked-value" uri="" />

    <channel type="mock" name="lock-table" uri="" />

</object>
"""


class EsrfSpecLockManagerMock(EsrfSpecLockManager):

    def __init__(self):
        from lxml import etree
        node = etree.fromstring(xml)
        self.__names = ["sx", "sy", "sz"]
        EsrfSpecLockManager.__init__(self, None, node)
        self._init()

    def _name_from_index(self, index):
        return self.__names[int(index)]

    def _update_index_mapping(self):
        pass

    def init(self):
        self.channels["locked-value"].set_value(1)
        table = {"0": "1", "1": "1", "2": "0"}
        self.channels["lock-table"].set_value(table)
        EsrfSpecLockManager.init(self)


class TestEsrfSpecLockManager(unittest.TestCase):

    def setUp(self):
        self.device = EsrfSpecLockManagerMock()

    def test_default(self):
        self.assertEqual(self.device.is_locked("sx"), True)
        self.assertEqual(self.device.is_locked("sz"), False)
        self.assertEqual(self.device.is_locked("unknown"), False)

    def test_change(self):
        table = {"0": "0", "1": "1", "2": "0"}
        self.device.channels["lock-table"].set_value(table)
        self.assertEqual(self.device.is_locked("sx"), False)

    def test_event(self):
        self.events = []

        def callback(event):
            self.events.append(event)

        self.device.connect("motor_sx_changed", callback)
        self.assertEqual(self.events, [True])

        table = {"0": "0", "1": "1", "2": "0"}
        self.device.channels["lock-table"].set_value(table)

        self.assertEqual(self.events, [True, False])
        del self.events

if __name__ == '__main__':
    unittest.main()

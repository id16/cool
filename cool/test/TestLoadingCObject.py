import unittest
from . import test

xml_with_embeded_object = """<object class="DiscreteMotor">
    <object class="MotorMock" role="motor">
        <config name="initial_value" value="5" />
        <config name="min_value" value="0" />
        <config name="max_value" value="10" />
        <config name="step" value="0.1" />
        <config name="delay" value="0.01" />
    </object>
</object>"""

xml_with_ref_object = """<object class="DiscreteMotor">
    <object href="TestLoadingCObject_motor" role="motor" />
</object>"""


xml_with_wrong_object = """<object class="DiscreteMotor">
    <object role="motor" />
</object>"""


class TestClassLoader(unittest.TestCase):

    def setUp(self):
        self.cool = test.setup()

    def test_none(self):
        klass = self.cool.core._get_class_from_classname(None)
        from cool.core import CObject
        self.assertEquals(klass, CObject)

    def test_empty(self):
        klass = self.cool.core._get_class_from_classname("")
        from cool.core import CObject
        self.assertEquals(klass, CObject)

    def test_default(self):
        klass = self.cool.core._get_class_from_classname("DiscreteMotor")
        from cool.control_objects.DiscreteMotor import DiscreteMotor
        self.assertEquals(klass, DiscreteMotor)

    def test_module(self):
        if __name__.startswith("cool."):
            klass = self.cool.core._get_class_from_classname("cool.test.GuineaPig")
        else:
            klass = self.cool.core._get_class_from_classname("test.GuineaPig")
        from .GuineaPig import GuineaPig
        self.assertEquals(klass, GuineaPig)

    def test_qualified_module_name(self):
        klass = self.cool.core._get_class_from_classname("cool.control_objects.DiscreteMotor")
        from cool.control_objects.DiscreteMotor import DiscreteMotor
        self.assertEquals(klass, DiscreteMotor)

    def test_qualified_class_name(self):
        klass = self.cool.core._get_class_from_classname("cool.control_objects.DiscreteMotor.DiscreteMotor")
        from cool.control_objects.DiscreteMotor import DiscreteMotor
        self.assertEquals(klass, DiscreteMotor)

    def test_missing_module(self):
        try:
            with test.inhibit_log("cool.core"):
                self.cool.core._get_class_from_classname("cool.control_objects2.DiscreteMotor")
        except ImportError:
            pass

    def test_missing_class(self):
        try:
            with test.inhibit_log("cool.core"):
                self.cool.core._get_class_from_classname("cool.core")
        except ImportError:
            pass

    def test_wrong_type(self):
        try:
            with test.inhibit_log("cool.core"):
                self.cool.core._get_class_from_classname("TestLoadingCObject.__name__")
        except Exception:
            pass


class TestLoadingCObject(unittest.TestCase):

    def setUp(self):
        self.cool = test.setup()

    def test_xmlstring_embeded(self):
        obj = self.cool.get_fromstring(xml_with_embeded_object)
        self.assertNotEquals(obj, None)
        from cool.control_objects.DiscreteMotor import DiscreteMotor
        self.assertEquals(obj.__class__, DiscreteMotor)
        self.assertNotEquals(obj.objects["motor"], None)
        self.assertNotEquals(obj.objects["motor"].__class__, "MotorMock")

    def test_xmlstring_ref(self):
        obj = self.cool.get_fromstring(xml_with_ref_object)
        self.assertNotEquals(obj, None)
        from cool.control_objects.DiscreteMotor import DiscreteMotor
        self.assertEquals(obj.__class__, DiscreteMotor)
        self.assertNotEquals(obj.objects["motor"], None)
        self.assertNotEquals(obj.objects["motor"].__class__, "MotorMock")

    def test_file_embeded(self):
        obj = self.cool.get("TestLoadingCObject_discrete_embeded")
        self.assertNotEquals(obj, None)
        from cool.control_objects.DiscreteMotor import DiscreteMotor
        self.assertEquals(obj.__class__, DiscreteMotor)
        self.assertNotEquals(obj.objects["motor"], None)
        self.assertNotEquals(obj.objects["motor"].__class__, "MotorMock")

    def test_file_ref(self):
        obj = self.cool.get("TestLoadingCObject_discrete")
        self.assertNotEquals(obj, None)
        from cool.control_objects.DiscreteMotor import DiscreteMotor
        self.assertEquals(obj.__class__, DiscreteMotor)
        self.assertNotEquals(obj.objects["motor"], None)
        self.assertNotEquals(obj.objects["motor"].__class__, "MotorMock")

    def test_load_relative_file_from_root(self):
        obj = self.cool.get("TestLoadingCObject_relative")
        self.assertIsNotNone(obj)
        self.assertIsNotNone(obj.objects["pig"])
        self.assertIsNotNone(obj.objects["sub-object"].objects["pig"])
        self.assertEquals(obj.objects["pig"].color(), "pink")
        self.assertEquals(obj.objects["sub-object"].objects["pig"].color(), "pink")

    def test_load_absolute_file_from_subdir(self):
        obj = self.cool.get("relative-dir/TestLoadingCObject_absolute")
        self.assertIsNotNone(obj)
        self.assertIsNotNone(obj.objects["pig"])
        self.assertIsNotNone(obj.objects["sub-object"].objects["pig"])
        self.assertEquals(obj.objects["pig"].color(), "pink")
        self.assertEquals(obj.objects["sub-object"].objects["pig"].color(), "pink")

    def test_load_relative_file_from_subdir(self):
        obj = self.cool.get("relative-dir/TestLoadingCObject_relative")
        self.assertIsNotNone(obj)
        self.assertIsNotNone(obj.objects["pig"])
        self.assertIsNotNone(obj.objects["sub-object"].objects["pig"])
        self.assertEquals(obj.objects["pig"].color(), "red")
        self.assertEquals(obj.objects["sub-object"].objects["pig"].color(), "red")

    def test_xmlstring_wrong(self):
        try:
            self.cool.get_fromstring(xml_with_wrong_object)
            self.fail()
        except:
            pass

if __name__ == '__main__':
    unittest.main()

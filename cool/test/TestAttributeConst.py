import unittest
import cool
from . import test

xml = """<object>

    <object role="quantity" class="ConstAttribute">
        <value quantity="10mm" />
    </object>

    <object role="integer" class="ConstAttribute">
        <value integer="10" />
    </object>

    <object role="float" class="ConstAttribute">
        <value float="10.50" />
    </object>

    <object role="string" class="ConstAttribute">
        <value string="foobar" />
    </object>

    <object role="boolean" class="ConstAttribute">
        <value boolean="true" />
    </object>

    <object role="none" class="ConstAttribute">
        <value none="true" />
    </object>

    <!-- Array of values -->
    <object role="quantity-list" class="ConstAttribute">
        <value quantity="-20mm" />
        <value quantity="0mm" />
    </object>

    <object role="one-element-list" list="true" class="ConstAttribute">
        <value boolean="false" />
    </object>

    <object role="empty-list" class="ConstAttribute">
    </object>

</object>
"""


class TestAttributeConst(unittest.TestCase):

    def setUp(self):
        cool = test.setup()
        self.device = cool.get_fromstring(xml)

    def test_declaration(self):
        value = self.device.objects["quantity"].value()
        self.assertEquals(value, cool.unit_registry.Quantity("10mm"))

        value = self.device.objects["integer"].value()
        self.assertEquals(value, 10)

        value = self.device.objects["float"].value()
        self.assertEquals(value, 10.50)

        value = self.device.objects["string"].value()
        self.assertEquals(value, "foobar")

        value = self.device.objects["boolean"].value()
        self.assertEquals(value, True)

        value = self.device.objects["none"].value()
        self.assertEquals(value, None)

        value = self.device.objects["quantity-list"].value()
        self.assertEquals(value, [cool.unit_registry.Quantity("-20mm"), cool.unit_registry.Quantity("0mm")])

        value = self.device.objects["one-element-list"].value()
        self.assertEquals(value, [False])

        value = self.device.objects["empty-list"].value()
        self.assertEquals(value, [])

if __name__ == '__main__':
    unittest.main()

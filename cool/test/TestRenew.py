import unittest
from . import test


class TestRenew(unittest.TestCase):

    def setUp(self):
        self.cool = test.setup()

    def test_not_same_device(self):
        obj1 = self.cool.get("GuineaPig")
        obj2 = self.cool.get("GuineaPig", from_cache=False)
        self.assertIsNot(obj1, obj2)

    def test_not_same_device_2(self):
        obj1 = self.cool.get("GuineaPig", from_cache=False)
        obj2 = self.cool.get("GuineaPig", from_cache=False)
        self.assertIsNot(obj1, obj2)

    def test_same_device(self):
        obj1 = self.cool.get("GuineaPig")
        obj2 = self.cool.get("GuineaPig")
        self.assertIs(obj1, obj2)

import time
import unittest
import cool
from cool.control_objects.Motor import MotorState
from . import test

xml_dimensionless = """<object class="DiscreteMotor">

    <!-- Name of the XML file (without the extension) containing
        description of the SpecMotor controlled -->
    <object class="MotorMock" role="motor">
        <config name="initial_value" value="5" />
        <config name="min_value" value="0" />
        <config name="max_value" value="10" />
        <config name="step" value="0.1" />
        <config name="delay" value="0.01" />
    </object>

    <!-- Set of positions identified by a name and an offset -->
    <position name="a" offset="0.5" />

    <!-- We can specify a delta in witch the position of the motor is still
        considered to be inside the named position.
        here, while the motor position is between 0.78 and 0.82 the discrete
        motor returns "b" as named position -->
    <position name="b" offset="0.8" delta="0.02" />

    <!-- ... -->
    <position name="c" offset="0.99" delta="0.02" />
    <position name="d" offset="3.1" delta="0.02" />
    <position name="origin" offset="5" />

    <!-- We can provide a human readable description -->
    <position name="e" offset="9.5" description="Gold coated Diamond filters" />

    <!-- And finally, we should avoid overlap of locations -->

</object>"""


xml_length = """<object class="DiscreteMotor">

    <!-- Motor have length units... -->
    <object class="MotorMock" role="motor">
        <config name="initial_value" value="5mm" />
        <config name="min_value" value="0mm" />
        <config name="max_value" value="10mm" />
        <config name="step" value="0.1mm" />
        <config name="delay" value="0.01" />
    </object>

    <!-- Then offest must have length unit too. -->
    <position name="a" offset="6mm" />
    <position name="origin" offset="5mm" />

</object>"""


class TestDiscreteMotor(unittest.TestCase):

    def setUp(self):
        self.cool = test.setup()
        self.motor = self.cool.get_fromstring(xml_dimensionless)

    def __wait_motor_finish_moving(self):
        for _i in range(0, 10):
            time.sleep(0.2)
            state = self.motor.state()
            if state != MotorState.MOVING:
                return
        raise Exception("Motor is still moving")

    def test_initial_position(self):
        self.assertEquals(self.motor.position_id(), "origin")
        self.assertEquals(self.motor.state(), MotorState.ON)

    def test_position_lower(self):
        self.motor.move_position_id("a")
        self.__wait_motor_finish_moving()
        self.assertEquals(self.motor.position_id(), "a")
        self.assertEquals(self.motor.debug_position(), 0.5)

    def test_position_upper(self):
        self.motor.move_position_id("e")
        self.__wait_motor_finish_moving()
        self.assertEquals(self.motor.position_id(), "e")
        self.assertEquals(self.motor.debug_position(), 9.5)

    def test_move_close_to_c(self):
        self.motor.debug_move(0.995)
        self.__wait_motor_finish_moving()
        self.assertEquals(self.motor.position_id(), "c")
        self.assertEquals(self.motor.state(), MotorState.ON)

    def test_move_close_to_d(self):
        self.motor.debug_move(3.09)
        self.__wait_motor_finish_moving()
        self.assertEquals(self.motor.position_id(), "d")
        self.assertEquals(self.motor.state(), MotorState.ON)

    def test_move_nowhere(self):
        self.motor.debug_move(2)
        self.__wait_motor_finish_moving()
        self.assertEquals(self.motor.position_id(), None)
        self.assertEquals(self.motor.state(), MotorState.ALARM)

    def test_events(self):
        requestedPosition = "a"
        self.lastPosition = None

        def callback(position):
            self.lastPosition = position

        self.motor.connect("positionIdChanged", callback)
        self.motor.move_position_id(requestedPosition)
        self.__wait_motor_finish_moving()
        self.assertEquals(self.lastPosition, requestedPosition)
        del self.lastPosition

    def test_state(self):
        requestedPosition = "c"
        self.motor.move_position_id(requestedPosition)
        self.assertEquals(self.motor.state(), MotorState.MOVING)
        self.__wait_motor_finish_moving()
        self.assertEquals(self.motor.state(), MotorState.ON)

    def test_state_events(self):
        requestedPosition = "d"
        self.lastState = None

        def callback(state):
            self.lastState = state

        expected = self.motor.state()
        self.motor.connect("stateChanged", callback)
        self.assertEquals(self.lastState, expected)
        self.motor.move_position_id(requestedPosition)
        self.__wait_motor_finish_moving()
        self.assertEquals(self.lastState, MotorState.ON)
        del self.lastState

    def test_position_events(self):
        requestedPosition = "d"
        self.lastPosition = None

        def callback(position):
            self.lastPosition = position

        self.motor.connect("positionChanged", callback)
        self.motor.move_position_id(requestedPosition)
        self.__wait_motor_finish_moving()
        self.assertNotEquals(self.lastPosition, None)
        self.assertEquals(self.lastPosition, 3.1)
        del self.lastPosition

    def test_add(self):
        pos = self.motor.create_position("new", 4.0, None)
        self.motor.debug_move(pos.offset)
        self.__wait_motor_finish_moving()
        self.assertEquals(self.motor.position_id(), None)
        self.motor.add_position_id(pos)
        self.assertEquals(self.motor.position_id(), pos.name)

    def test_add_without_delta(self):
        pos = self.motor.create_position("new", 4.0, None)
        del pos.delta
        self.motor.add_position_id(pos)

    def test_add_wrong(self):
        pos = self.motor.create_position("new", 4.0, None)
        del pos.offset
        try:
            self.motor.add_position_id(pos)
            self.fail()
        except:
            self.assertTrue(True)

    def test_remove(self):
        position = "origin"
        self.assertEquals(self.motor.position_id(), position)
        self.motor.remove_position_id(position)
        self.assertEquals(self.motor.position_id(), None)

    def test_config(self):
        config = self.motor.get_configuration()
        self.assertNotEquals(config, None)
        self.assertEquals(len(config), 6)
        for c in config:
            if c.name == "origin":
                config = c
                break
        self.assertEquals(config.name, "origin")
        self.assertEquals(config.offset, 5)

    def test_config_events(self):
        self.lastConfig = None

        def callback(config):
            self.lastConfig = config

        self.motor.connect("configurationChanged", callback)
        self.assertNotEquals(self.lastConfig, None)
        self.assertEquals(len(self.lastConfig), 6)
        del self.lastConfig

    def test_config_has_description(self):
        config = self.motor.get_configuration()
        found = False
        for c in config:
            if c.name == "e":
                if "Gold" in c.description:
                    found = True
                    break
        self.assertTrue(found)

    def test_store(self):
        # edit the config
        config = self.motor.get_configuration()
        mapping = {}
        for c in config:
            mapping[c.name] = c
        del mapping["d"]
        pos = self.motor.create_position("new", 9.0)
        mapping[pos.name] = pos
        mapping["a"].offset = 5.0
        self.motor.set_configuration(mapping.values())
        self.motor.store()
        result = self.motor.config.tostring()

        # check the xml
        motor = self.cool.get_fromstring(result)
        config = motor.get_configuration()
        mapping = {}
        for c in config:
            mapping[c.name] = c

        self.assertFalse("d" in mapping)
        self.assertTrue("new" in mapping)
        self.assertAlmostEquals(mapping["new"].offset, pos.offset)
        self.assertAlmostEquals(mapping["a"].offset, 5.0)

    def test_motor_disabled(self):
        self.internal_motor = self.motor.objects["motor"]
        self.internal_motor._force_disabled()
        self.assertEquals(self.motor.state(), MotorState.DISABLED)

    def test_motor_fault(self):
        self.internal_motor = self.motor.objects["motor"]
        self.internal_motor._force_fault()
        self.assertEquals(self.motor.state(), MotorState.FAULT)

    def test_initial_unreachable_position(self):
        unreachable = self.motor.unreachable_position_ids()
        self.assertEquals(unreachable, [])

    def test_unreachable_position(self):
        mini, maxi = cool.unit_registry.Quantity("0.6"), cool.unit_registry.Quantity("8.8")
        self.motor.objects["motor"].set_limits((mini, maxi,))
        unreachable = self.motor.unreachable_position_ids()
        self.assertEquals(unreachable, ["a", "e"])


class TestDiscreteMotorLength(unittest.TestCase):

    def setUp(self):
        self.cool = test.setup()
        self.motor = self.cool.get_fromstring(xml_length)

    def __wait_motor_finish_moving(self):
        for _i in range(0, 10):
            time.sleep(0.2)
            state = self.motor.state()
            if state != MotorState.MOVING:
                return
        raise Exception("Motor is still moving")

    def test_position_lower(self):
        self.motor.move_position_id("a")
        self.__wait_motor_finish_moving()
        self.assertEquals(self.motor.position_id(), "a")
        expectedPosition = cool.unit_registry.Quantity("6mm")
        self.assertEquals(self.motor.debug_position(), expectedPosition)

    def test_position_events(self):
        requestedPosition = "a"
        expectedPosition = cool.unit_registry.Quantity("6mm")
        self.lastPosition = None

        def callback(position):
            self.lastPosition = position

        self.motor.connect("positionChanged", callback)
        self.motor.move_position_id(requestedPosition)
        self.__wait_motor_finish_moving()
        self.assertNotEquals(self.lastPosition, None)
        self.assertEquals(self.lastPosition, expectedPosition)
        del self.lastPosition

    def test_config(self):
        config = self.motor.get_configuration()
        expectedPosition = cool.unit_registry.Quantity("5mm")
        self.assertNotEquals(config, None)
        self.assertEquals(len(config), 2)
        for c in config:
            if c.name == "origin":
                config = c
                break
        self.assertEquals(config.name, "origin")
        self.assertEquals(config.offset, expectedPosition)

if __name__ == '__main__':
    unittest.main()

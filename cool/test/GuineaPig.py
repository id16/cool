from cool.core import CObject
import threading


class GuineaPigException(Exception):
    pass


class GuineaPig(CObject):
    def __init__(self, *args, **kwargs):
        CObject.__init__(self, *args, **kwargs)

    def init(self):
        c = self.config["config[@name='color']/@value"]
        if len(c) == 1:
            self._color = c[0]
        elif len(c) == 0:
            self._color = "pink"
        else:
            raise Exception("Only one color expected")

    def store(self):
        CObject.store(self)
        node = self.config["config[@name='color']"][0]
        if node is not None:
            node.set("value", self._color)
        else:
            self.config.append_string("<config name='color' value='%s' />" % self.color)

    def set_color(self, color):
        if color == self._color:
            return
        self._color = color
        self.emit("colorChanged", self._color)

    def color(self):
        return self._color

    def answer(self, question):
        raise GuineaPigException("Guinea Pig do not understand your question")

    def thread_id(self):
        return id(threading.current_thread())

    def connect_notify(self, signal):
        if signal == "colorChanged":
            self.emit("colorChanged", self._color)
        else:
            raise Exception("Unsupported signal %s" % signal)

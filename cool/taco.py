import sys
import traceback
import copy
from .command_channel import Command
from .command_channel import Channel
from . import Poller
from . import TacoDevice_MTSafe as TacoDevice


class TacoCommand(Command):
    def __init__(self, name, command, taco_device_name, username=None, args=None, dc=False, **kwargs):
        Command.__init__(self, name, username, **kwargs)

        self.command = command
        self.deviceName = taco_device_name
        self._dc = dc
        if args is None:
            self.arglist = ()
        else:
            # not very nice...
            args = str(args)
            if not args.endswith(","):
                args += ","
            self.arglist = eval("(" + args + ")")

    def init(self):
        try:
            self.device = TacoDevice.TacoDevice(self.deviceName, dc=self._dc)
        except:
            self.device = None
        self.hardware_connected(self.device is not None and self.device.imported)

    def _execute(self, *args, **kwargs):
        if len(args) > 0 and len(self.arglist) > 0:
            raise RuntimeError("Cannot execute %s" % str(self.name()))
        elif len(args) == 0 and len(self.arglist) > 0:
            args = self.arglist

        if self.device is None:
            self.init()

        if self.device is not None and self.device.imported:
            try:
                ret = getattr(self.device, self.command)(*args)
            except Exception as e:
                _exception, _error_string, tb = sys.exc_info()
                full_tb = traceback.extract_tb(tb)
                exception_tb = "".join(traceback.format_list(full_tb))
                raise RuntimeError("%s: an error occured when calling Taco command %s:\n%s\nTraceback:\n%s" % (self.name(), self.command, e, exception_tb))
            else:
                return ret
        raise RuntimeError("Device is not imported")

    def onPollingError(self, exception, poller_id):
        self.init()
        poller = Poller.get_poller(poller_id)
        if poller is not None:
            try:
                poller.restart(1000)
            except:
                pass

    def poll(self, callback, argumentsList=(), pollingTime=500, compare=True):
        poll_cmd = getattr(self.device, self.command)

        Poller.poll(poll_cmd, copy.deepcopy(argumentsList), pollingTime, callback, self.onPollingError, compare)

    def abort(self):
        pass


class TacoChannel(Channel):
    """Emulation of a 'Taco channel' = a Taco command + polling"""
    def __init__(self, name, command, taco_device_name, username=None, polling=None, args=None, **kwargs):
        Channel.__init__(self, name, username, **kwargs)

        self._polling = polling
        self.command = TacoCommand(name, command, taco_device_name, username, args, False, **kwargs)
        self.command.connect("__hw_connected__", self.hardware_connected)

    def init(self):
        try:
            self.polling = int(self._polling)
        except:
            self.polling = None
        else:
            self.command.init()
            self.command.poll(self.update, self.command.arglist, self.polling)

    def value(self):
        return self.command()

from cool.command_channel import Channel
from cool.command_channel import Command


class MockChannel(Channel):
    """Simulate a channel programmatically.

    A default value can be initialized at start up.

    Use `set_value` to update the value and simulate changes on the
    hardware side.

    TODO make sure we can support hardware_connected and hardware_disconnected
    commands
    """

    def __init__(self, name, username=None, value=None, connected=True, **kwargs):
        Channel.__init__(self, name, username, **kwargs)
        self._value = value
        if connected:
            self.hardware_connected()

    def value(self):
        return self._value

    def set_value(self, value):
        self._value = value
        self.update(value)


class MockCommand(Command):
    """Simulate a command programmatically.

    Use set_callback to define a programmatic associated command. It will be
    executed every time the command will be called.

    TODO make sure we can support hardware_connected and hardware_disconnected
    commands
    """

    def __init__(self, name, username=None, callback=None, **kwargs):
        Command.__init__(self, name, username, **kwargs)
        self.__callback = callback
        self.hardware_connected()

    def set_callback(self, callback):
        self.__callback = callback

    def _execute(self, *args):
        if self.__callback:
            return self.__callback(*args)

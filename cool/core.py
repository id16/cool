import weakref
import new
import logging
from lxml import etree
from .dispatcher import dispatcher
import thread
import importlib
import inspect
import cool
from cool import config
from cool.thread import objectProxy


_logger = logging.getLogger(__name__)

OBJECTS_CACHE = weakref.WeakValueDictionary()


def get(cobject_name, in_thread=False, from_cache=True):
    if from_cache:
        try:
            return OBJECTS_CACHE[cobject_name]
        except:
            pass

    filename = config.xmlfile_path(cobject_name)
    try:
        xml_config_tree = etree.parse(filename)
    except:
        _logger.error("Error while parsing object %s (filename %s)", cobject_name, filename)
        raise
    node = xml_config_tree.getroot()
    o = get_fromnode(cobject_name, node, in_thread)
    if o:
        OBJECTS_CACHE[cobject_name] = o
    return o


def get_fromstring(xml, in_thread=False):
    node = etree.fromstring(xml)
    return get_fromnode(None, node, in_thread)


def _get_class_from_classname(class_name):
    """Cool device class loader

    Return a class from a cool device class name.

    The class name can look at this 4 patterns, checked in this order:
    1) '' -> class CObject from cool
    2) singleword -> class singleword from cool.control_objects.singleword
    3) modulepath.modulename -> class modulename from modulepath.modulename
    4) modulepath.classname -> class classname from modulepath

    - If the class name do not match anything an ImportError exception in raised.
    - If the found object is not a class it raise a ImportError

    :type class_name: str
    :rtype: class
    """
    if class_name is None or class_name == "":
        return CObject

    loading_paths = []
    loading_paths.append(("cool.control_objects." + class_name, class_name))
    if "." in class_name:
        path, suffix = class_name.rsplit(".", 1)
        loading_paths.append((class_name, suffix))
        if path != "":
            loading_paths.append((path, suffix))
    else:
        loading_paths.append((class_name, class_name))

    errors = []

    for qualified in loading_paths:
        module_name, class_name = qualified
        try:
            module = importlib.import_module(module_name)
        except ImportError as e:
            errors.append((module_name, e))
            continue

        try:
            klass = getattr(module, class_name)
        except AttributeError as e:
            _logger.debug("Error while reaching class name %s from module %s", module, class_name)
            raise ImportError(str(e))

        if not inspect.isclass(klass):
            raise ImportError("Class expected but %s found" % type(klass))

        return klass

    for module_name, e in errors:
        _logger.error("Error while importing module %s", module_name, exc_info=e)

    raise ImportError("Cool device class '%s' not found" % class_name)


def get_fromnode(cobject_name, node, in_thread, parent_object=None):
    class_name = node.get("class")
    klass = _get_class_from_classname(class_name)

    if in_thread:
        o = thread.get_proxy(klass, cobject_name, node)
    else:
        o = klass(cobject_name, node)
    if parent_object is not None:
        o._set_root_document(parent_object.config.get_root_document())
    o._init()

    return o


class CObjectMeta(type):
    def __instancecheck__(self, other):
        """Allow to check threadsafe-proxy-object transparently with isinstance.
        It must be in a metaclass."""
        if other.__class__ == objectProxy:
            return issubclass(other._class, self)
        return issubclass(other.__class__, self)


class CObject:
    __metaclass__ = CObjectMeta

    def __init__(self, name=None, xml_tree=None):
        self.config = config.Configuration(name, xml_tree)
        self.commands = {}
        self.channels = {}
        self.objects = {}
        self.__ready = None

    def _set_root_document(self, root_document):
        self.config.root_document = root_document

    def _init(self):
        node = self.config.tree()
        cmds = node.xpath("command")
        chans = node.xpath("channel")

        try:
            default_uri = node.xpath("uri[1]")[0].text
        except:
            default_uri = None

        for cmd in cmds:
            attribs = dict(cmd.attrib)

            type_ = attribs["type"]
            name = attribs["name"]
            cmd_name = name
            uri = attribs.get("uri", default_uri)
            try:
                del attribs["uri"]
            except KeyError:
                pass
            del attribs["type"]
            del attribs["name"]

            add_command(self, type_, name, uri, cmd_name, **attribs)

        for chan in chans:
            attribs = dict(chan.attrib)

            type_ = attribs["type"]
            name = attribs["name"]
            chan_name = name
            uri = attribs.get("uri", default_uri)
            try:
                del attribs["uri"]
            except KeyError:
                pass
            del attribs["type"]
            del attribs["name"]

            add_channel(self, type_, name, uri, chan_name, **attribs)

        for object_elt in self.config["object"]:
            role = object_elt.attrib["role"]

            if "href" in object_elt.attrib:
                object_ref = object_elt.attrib["href"]
                if object_ref.startswith("/"):
                    # it is an absolute path
                    object_ref = object_ref[1:]
                else:
                    root_document = self.config.get_root_document()
                    if root_document is not None:
                        # extract directory of the document
                        dirs = root_document.split("/")
                        dirs.pop()
                        dirs.append(object_ref)
                        object_ref = str.join("/", dirs)
                    else:
                        # else it is an absolute ref
                        pass
                    pass
                obj = get(object_ref)
                self.objects[role] = obj
            else:
                obj = get_fromnode(None, object_elt, False, self)
                self.objects[role] = obj

        self.init()

    def init(self):
        # to be overwritten
        pass

    def store(self):
        """Store local configuration into the configuration object. To be overwritten."""
        # store children configuration
        for o in self.objects.values():
            o.store()

    def save(self, save_references=True):
        """Save the current objects and its references into XML files"""
        self.store()
        self.__save(save_references)

    def __save(self, save_references):
        if save_references:
            for o in self.objects.values():
                o.__save(True)
        if self.config.is_document():
            self.config.save()

    def connect_notify(self, signal_name):
        pass

    def username(self):
        try:
            return self.config["@username"][0]
        except:
            return self.name()

    def emit(self, signal, *args):
        dispatcher.send(signal, self, *args)

    def connect(self, signal_name, callable_):
        dispatcher.connect(callable_, signal_name, self)
        self.connect_notify(signal_name)

    def disconnect(self, signal_name, callable_):
        dispatcher.disconnect(callable_, signal_name, self)

    def add_command(self, *args, **kwargs):
        return add_command(self, *args, **kwargs)

    def add_channel(self, *args, **kwargs):
        update_callback = kwargs.get("update_callback", None)
        if update_callback is not None:
            del kwargs["update_callback"]
        chan = add_channel(self, *args, **kwargs)
        if update_callback is not None:
            chan.connect("update", update_callback)
        return chan


def add_command(object_, type_, name, uri, cmd_name, **attribs):
    if uri is None:
        raise RuntimeError("Cannot add command `%s': invalid URI (object: %r)" % (name, object_))

    new_command = None
    cmd_name = attribs.get("call", cmd_name)
    if type_ == "spec":
        SpecCommand = __import__('spec', globals(), locals(), ['']).SpecCommand
        new_command = SpecCommand(name, cmd_name, uri, **attribs)
    elif type_ == "taco":
        TacoCommand = __import__('taco', globals(), locals(), ['']).TacoCommand
        new_command = TacoCommand(name, cmd_name, uri, **attribs)
    elif type_ == "tango":
        TangoCommand = __import__('tango', globals(), locals(), ['']).TangoCommand
        new_command = TangoCommand(name, cmd_name, uri, **attribs)
    elif type_ == "mock":
        MockCommand = __import__('mock', globals(), locals(), ['']).MockCommand
        new_command = MockCommand(name, cmd_name, uri, **attribs)
    else:
        raise TypeError(type_)

    slot = attribs.get("slot")
    if slot is not None:
        # if slot is defined, slot has to be added => it will call the command
        try:
            getattr(object_, slot)
        except AttributeError:
            def command_call(self, *args, **kwargs):
                return self.commands[name](*args, **kwargs)
            setattr(object_, slot, new.instancemethod(command_call, object_, object_.__class__))
        else:
            raise RuntimeError("Cannot overwrite control object object '%s` member: '%s`" % (object_.name(), slot))

    object_.commands[name] = new_command

    return new_command


def add_channel(object_, type_, name, uri, chan_name, **attribs):
    new_channel = None
    chan_name = attribs.get("call", chan_name)

    if type_ == "spec":
        SpecChannel = __import__('spec', globals(), locals(), ['']).SpecChannel
        new_channel = SpecChannel(name, chan_name, uri, **attribs)
    elif type_ == "taco":
        TacoChannel = __import__('taco', globals(), locals(), ['']).TacoChannel
        new_channel = TacoChannel.TacoChannel(name, chan_name, uri, **attribs)
    elif type_ == "tango":
        TangoChannel = __import__('tango', globals(), locals(), ['']).TangoChannel
        new_channel = TangoChannel(name, chan_name, uri, **attribs)
    elif type_ == "mock":
        MockChannel = __import__('mock', globals(), locals(), ['']).MockChannel
        value = None
        if 'value' in attribs:
            value = eval(attribs['value'])
            del attribs['value']
        new_channel = MockChannel(name, chan_name, value=value, **attribs)
    else:
        raise TypeError(type_)

    signal = attribs.get("signal")
    if signal is not None:
        # in case of update, this signal has to be emitted
        def emit_signal(value, signal_name=signal, obj_ref=weakref.ref(object_)):
            obj = obj_ref()
            if obj is not None:
                obj.emit(signal_name, value)

        new_channel.connect("update", emit_signal)

        setattr(object_, "_%d_emit_signal" % id(emit_signal), new.instancemethod(emit_signal, object_, object_.__class__))

    slot = attribs.get("slot")
    if slot is not None:
        # if slot is defined, slot has to be added => it will call the command
        try:
            getattr(object_, slot)
        except AttributeError:
            def channel_read(self, *args, **kwargs):
                return self.channels[name].value()
            setattr(object_, slot, new.instancemethod(channel_read, object_, object_.__class__))
        else:
            raise RuntimeError("Cannot overwrite control object object '%s` member: '%s`" % (object_.name(), slot))

    object_.channels[name] = new_channel

    return new_channel
